package com.coreit.coreutils;

import com.coreit.coreutils.logging.Console;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Ramadan
 */
public class StringUtils {

    private static final int BYTE_RANGE = 256;
    private static byte[] allBytes = new byte[256];

    private static char[] byteToChars = new char[256];
    private static Method toPlainStringMethod;
    static final int WILD_COMPARE_MATCH_NO_WILD = 0;
    static final int WILD_COMPARE_MATCH_WITH_WILD = 1;
    static final int WILD_COMPARE_NO_MATCH = -1;

    /**
     *
     * @param decimal
     * @return
     */
    public static String consistentToString(BigDecimal decimal) {
        if (decimal == null) {
            return null;
        }

        if (toPlainStringMethod != null) {
            try {
                return (String) toPlainStringMethod.invoke(decimal, (Object[]) null);
            } catch (InvocationTargetException | IllegalAccessException invokeEx) {
            }
        }
        return decimal.toString();
    }

    /**
     *
     * @param string
     * @param var
     * @return
     */
    public static boolean hasMultipleOccurence(String string, char var) {
        int occurence = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == var) {
                occurence++;
            }
            if (occurence > 1) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param str
     * @param ch
     * @return
     */
    public static int findFirstBackward(String str, char ch) {
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) == ch) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * @param content
     * @param item
     * @return
     */
    public static final boolean containsCi(String content, String item) {
        return content.toLowerCase().contains(item.toLowerCase());
    }

    public static String delimSubstr(int start,String delimiter,String content){
        int didx = content.indexOf(delimiter);
        return content.substring(start, didx);
    }
    /**
     *
     * @param args
     */
    public static void main(String args[]) {
        String row = "hello world this is a test";
        Console.log(delimSubstr(0," ",row));
    }

    /**
     *
     * @param string
     * @param var
     * @return
     */
    public static int countOccurence(String string, char var) {
        int occurence = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == var) {
                occurence++;
            }
        }
        return occurence;
    }

    /**
     *
     * @param byteBuffer
     * @param length
     * @return
     */
    public static final String dumpAsHex(byte[] byteBuffer, int length) {
        StringBuilder outputBuf = new StringBuilder(length * 4);

        int p = 0;
        int rows = length / 8;

        for (int i = 0; (i < rows) && (p < length); i++) {
            int ptemp = p;

            for (int j = 0; j < 8; j++) {
                String hexVal = Integer.toHexString(byteBuffer[ptemp] & 0xFF);

                if (hexVal.length() == 1) {
                    hexVal = "0" + hexVal;
                }

                outputBuf.append(hexVal).append(" ");
                ptemp++;
            }

            outputBuf.append("    ");

            for (int j = 0; j < 8; j++) {
                if ((byteBuffer[p] > 32) && (byteBuffer[p] < 127)) {
                    outputBuf.append((char) byteBuffer[p]).append(" ");
                } else {
                    outputBuf.append(". ");
                }

                p++;
            }

            outputBuf.append("\n");
        }

        int n = 0;

        for (int i = p; i < length; i++) {
            String hexVal = Integer.toHexString(byteBuffer[i] & 0xFF);

            if (hexVal.length() == 1) {
                hexVal = "0" + hexVal;
            }

            outputBuf.append(hexVal).append(" ");
            n++;
        }

        for (int i = n; i < 8; i++) {
            outputBuf.append("   ");
        }

        outputBuf.append("    ");

        for (int i = p; i < length; i++) {
            if ((byteBuffer[i] > 32) && (byteBuffer[i] < 127)) {
                outputBuf.append((char) byteBuffer[i]).append(" ");
            } else {
                outputBuf.append(". ");
            }
        }

        outputBuf.append("\n");

        return outputBuf.toString();
    }

    public static String trimAll(String content) {
        String ret = "";
        for (int i = 0; i < content.length(); i++) {
            if (content.charAt(i) != ' ') {
                ret += content.charAt(i);
            }
        }
        return ret;
    }

    private static boolean endsWith(byte[] dataFrom, String suffix) {
        for (int i = 1; i <= suffix.length(); i++) {
            int dfOffset = dataFrom.length - i;
            int suffixOffset = suffix.length() - i;
            if (dataFrom[dfOffset] != suffix.charAt(suffixOffset)) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param origBytes
     * @param origString
     * @param offset
     * @param length
     * @return
     */
    public static byte[] escapeEasternUnicodeByteStream(byte[] origBytes, String origString, int offset, int length) {
        if ((origBytes == null) || (origBytes.length == 0)) {
            return origBytes;
        }

        int bytesLen = origBytes.length;
        int bufIndex = 0;
        int strIndex = 0;

        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream(bytesLen);
        while (true) {
            if (origString.charAt(strIndex) == '\\') {
                bytesOut.write(origBytes[(bufIndex++)]);
            } else {
                int loByte = origBytes[bufIndex];

                if (loByte < 0) {
                    loByte += 256;
                }

                bytesOut.write(loByte);

                if (loByte >= 128) {
                    if (bufIndex < bytesLen - 1) {
                        int hiByte = origBytes[(bufIndex + 1)];

                        if (hiByte < 0) {
                            hiByte += 256;
                        }

                        bytesOut.write(hiByte);
                        bufIndex++;

                        if (hiByte == 92) {
                            bytesOut.write(hiByte);
                        }
                    }
                } else if ((loByte == 92)
                        && (bufIndex < bytesLen - 1)) {
                    int hiByte = origBytes[(bufIndex + 1)];

                    if (hiByte < 0) {
                        hiByte += 256;
                    }

                    if (hiByte == 98) {
                        bytesOut.write(92);
                        bytesOut.write(98);
                        bufIndex++;
                    }

                }

                bufIndex++;
            }

            if (bufIndex >= bytesLen) {
                break;
            }

            strIndex++;
        }

        return bytesOut.toByteArray();
    }

    /**
     *
     * @param searchIn
     * @return
     */
    public static char firstNonWsCharUc(String searchIn) {
        return firstNonWsCharUc(searchIn, 0);
    }

    /**
     *
     * @param cs
     * @return
     */
    public static String[] splitByLines(CharSequence cs) {
        return splitByLines(cs.toString());
    }

    /**
     *
     * @param s
     * @return
     */
    public static String[] splitByLines(String s) {
        return s.split(NEW_LINE_PATTERN, -1);
    }

    /**
     *
     * @param searchIn
     * @param startAt
     * @return
     */
    public static char firstNonWsCharUc(String searchIn, int startAt) {
        if (searchIn == null) {
            return '\000';
        }

        int length = searchIn.length();

        for (int i = startAt; i < length; i++) {
            char c = searchIn.charAt(i);

            if (!Character.isWhitespace(c)) {
                return Character.toUpperCase(c);
            }
        }

        return '\000';
    }

    /**
     *
     * @param dString
     * @return
     */
    public static final String fixDecimalExponent(String dString) {
        int ePos = dString.indexOf("E");

        if (ePos == -1) {
            ePos = dString.indexOf("e");
        }

        if ((ePos != -1)
                && (dString.length() > ePos + 1)) {
            char maybeMinusChar = dString.charAt(ePos + 1);

            if ((maybeMinusChar != '-') && (maybeMinusChar != '+')) {
                StringBuffer buf = new StringBuffer(dString.length() + 1);
                buf.append(dString.substring(0, ePos + 1));
                buf.append('+');
                buf.append(dString.substring(ePos + 1, dString.length()));
                dString = buf.toString();
            }

        }

        return dString;
    }

    /**
     *
     * @param buf
     * @param offset
     * @param endPos
     * @return
     * @throws NumberFormatException
     */
    public static int getInt(byte[] buf, int offset, int endPos)
            throws NumberFormatException {
        int base = 10;

        int s = offset;

        while ((Character.isWhitespace((char) buf[s])) && (s < endPos)) {
            s++;
        }

        if (s == endPos) {
            throw new NumberFormatException(new String(buf));
        }

        boolean negative = false;

        if ((char) buf[s] == '-') {
            negative = true;
            s++;
        } else if ((char) buf[s] == '+') {
            s++;
        }

        int save = s;

        int cutoff = 2147483647 / base;
        int cutlim = 2147483647 % base;

        if (negative) {
            cutlim++;
        }

        boolean overflow = false;

        int i = 0;

        for (; s < endPos; s++) {
            char c = (char) buf[s];

            if (Character.isDigit(c)) {
                c = (char) (c - '0');
            } else {
                if (!Character.isLetter(c)) {
                    break;
                }
                c = (char) (Character.toUpperCase(c) - 'A' + 10);
            }

            if (c >= base) {
                break;
            }

            if ((i > cutoff) || ((i == cutoff) && (c > cutlim))) {
                overflow = true;
            } else {
                i *= base;
                i += c;
            }
        }

        if (s == save) {
            throw new NumberFormatException(new String(buf));
        }

        if (overflow) {
            throw new NumberFormatException(new String(buf));
        }

        return negative ? -i : i;
    }

    /**
     *
     * @param buf
     * @return
     * @throws NumberFormatException
     */
    public static int getInt(byte[] buf) throws NumberFormatException {
        return getInt(buf, 0, buf.length);
    }

    /**
     *
     * @param buf
     * @return
     * @throws NumberFormatException
     */
    public static long getLong(byte[] buf) throws NumberFormatException {
        int base = 10;

        int s = 0;

        while ((Character.isWhitespace((char) buf[s])) && (s < buf.length)) {
            s++;
        }

        if (s == buf.length) {
            throw new NumberFormatException(new String(buf));
        }

        boolean negative = false;

        if ((char) buf[s] == '-') {
            negative = true;
            s++;
        } else if ((char) buf[s] == '+') {
            s++;
        }

        int save = s;

        long cutoff = 9223372036854775807L / base;
        long cutlim = (int) (9223372036854775807L % base);

        if (negative) {
            cutlim += 1L;
        }

        boolean overflow = false;
        long i = 0L;

        for (; s < buf.length; s++) {
            char c = (char) buf[s];

            if (Character.isDigit(c)) {
                c = (char) (c - '0');
            } else {
                if (!Character.isLetter(c)) {
                    break;
                }
                c = (char) (Character.toUpperCase(c) - 'A' + 10);
            }

            if (c >= base) {
                break;
            }

            if ((i > cutoff) || ((i == cutoff) && (c > cutlim))) {
                overflow = true;
            } else {
                i *= base;
                i += c;
            }
        }

        if (s == save) {
            throw new NumberFormatException(new String(buf));
        }

        if (overflow) {
            throw new NumberFormatException(new String(buf));
        }

        return negative ? -i : i;
    }

    /**
     *
     * @param buf
     * @return
     * @throws NumberFormatException
     */
    public static short getShort(byte[] buf) throws NumberFormatException {
        short base = 10;

        int s = 0;

        while ((Character.isWhitespace((char) buf[s])) && (s < buf.length)) {
            s++;
        }

        if (s == buf.length) {
            throw new NumberFormatException(new String(buf));
        }

        boolean negative = false;

        if ((char) buf[s] == '-') {
            negative = true;
            s++;
        } else if ((char) buf[s] == '+') {
            s++;
        }

        int save = s;

        short cutoff = (short) (32767 / base);
        short cutlim = (short) (32767 % base);

        if (negative) {
            cutlim = (short) (cutlim + 1);
        }

        boolean overflow = false;
        short i = 0;

        for (; s < buf.length; s++) {
            char c = (char) buf[s];

            if (Character.isDigit(c)) {
                c = (char) (c - '0');
            } else {
                if (!Character.isLetter(c)) {
                    break;
                }
                c = (char) (Character.toUpperCase(c) - 'A' + 10);
            }

            if (c >= base) {
                break;
            }

            if ((i > cutoff) || ((i == cutoff) && (c > cutlim))) {
                overflow = true;
            } else {
                i = (short) (i * base);
                i = (short) (i + c);
            }
        }

        if (s == save) {
            throw new NumberFormatException(new String(buf));
        }

        if (overflow) {
            throw new NumberFormatException(new String(buf));
        }

        return negative ? (short) -i : i;
    }

    /**
     *
     * @param start
     * @param source
     * @param item
     * @return
     */
    public static int indexOfIgnoreCase(int start, String source, String item) {
        String slower = source.toLowerCase();
        String ilower = item.toLowerCase();
        if (slower.length() >= start) {
            return slower.indexOf(ilower, start);
        }
        return -1;
    }

    /**
     *
     * @param startingPosition
     * @param searchIn
     * @param searchFor
     * @return
     */
    public static final int indexOfIgnoreCaseEx(int startingPosition, String searchIn, String searchFor) {
        if ((searchIn == null) || (searchFor == null) || (startingPosition > searchIn.length())) {
            return -1;
        }

        int patternLength = searchFor.length();
        int stringLength = searchIn.length();
        int stopSearchingAt = stringLength - patternLength;

        int i = startingPosition;

        if (patternLength == 0) {
            return -1;
        }

        char firstCharOfPatternUc = Character.toUpperCase(searchFor.charAt(0));
        char firstCharOfPatternLc = Character.toLowerCase(searchFor.charAt(0));

        while ((i < stopSearchingAt) && (Character.toUpperCase(searchIn.charAt(i)) != firstCharOfPatternUc) && (Character.toLowerCase(searchIn.charAt(i)) != firstCharOfPatternLc)) {
            i++;
        }

        if (i > stopSearchingAt) {
            return -1;
        }

        int j = i + 1;
        int end = j + patternLength - 1;

        int k = 1;
        while (true) {
            if (j >= end) {
                break;
            }
            int searchInPos = j++;
            int searchForPos = k++;

            if (Character.toUpperCase(searchIn.charAt(searchInPos)) != Character.toUpperCase(searchFor.charAt(searchForPos))) {
                i++;

                break;
            }

            if (Character.toLowerCase(searchIn.charAt(searchInPos)) != Character.toLowerCase(searchFor.charAt(searchForPos))) {
                i++;

                break;
            }
        }

        label209:
        return i;
    }

    /**
     *
     * @param searchIn
     * @param searchFor
     * @return
     */
    public static final int indexOfIgnoreCase(String searchIn, String searchFor) {
        return indexOfIgnoreCase(0, searchIn, searchFor);
    }

    /**
     *
     * @param startAt
     * @param src
     * @param target
     * @param marker
     * @param markerCloses
     * @param allowBackslashEscapes
     * @return
     */
    public static int indexOfIgnoreCaseRespectMarker(int startAt, String src, String target, String marker, String markerCloses, boolean allowBackslashEscapes) {
        char contextMarker = '\000';
        boolean escaped = false;
        int markerTypeFound = -1;
        int srcLength = src.length();
        int ind = 0;

        for (int i = startAt; i < srcLength; i++) {
            char c = src.charAt(i);

            if ((allowBackslashEscapes) && (c == '\\')) {
                escaped = !escaped;
            } else if ((markerTypeFound != -1) && (c == markerCloses.charAt(markerTypeFound)) && (!escaped)) {
                contextMarker = '\000';
                markerTypeFound = -1;
            } else if (((ind = marker.indexOf(c)) != -1) && (!escaped) && (contextMarker == 0)) {
                markerTypeFound = ind;
                contextMarker = c;
            } else if ((c == target.charAt(0)) && (!escaped) && (contextMarker == 0)) {
                if (indexOfIgnoreCase(i, src, target) != -1) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     *
     * @param startAt
     * @param src
     * @param target
     * @param quoteChar
     * @param allowBackslashEscapes
     * @return
     */
    public static int indexOfIgnoreCaseRespectQuotes(int startAt, String src, String target, char quoteChar, boolean allowBackslashEscapes) {
        char contextMarker = '\000';
        boolean escaped = false;

        int srcLength = src.length();

        for (int i = startAt; i < srcLength; i++) {
            char c = src.charAt(i);

            if ((allowBackslashEscapes) && (c == '\\')) {
                escaped = !escaped;
            } else if ((c == contextMarker) && (!escaped)) {
                contextMarker = '\000';
            } else if ((c == quoteChar) && (!escaped) && (contextMarker == 0)) {
                contextMarker = c;
            } else if (((Character.toUpperCase(c) == Character.toUpperCase(target.charAt(0))) || (Character.toLowerCase(c) == Character.toLowerCase(target.charAt(0)))) && (!escaped) && (contextMarker == 0)) {
                if (startsWithIgnoreCase(src, i, target)) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     *
     * @param stringToSplit
     * @param delimitter
     * @param trim
     * @return
     */
    public static final List<String> split(String stringToSplit, String delimitter, boolean trim) {
        if (stringToSplit == null) {
            return new ArrayList();
        }

        if (delimitter == null) {
            throw new IllegalArgumentException();
        }

        StringTokenizer tokenizer = new StringTokenizer(stringToSplit, delimitter, false);

        List splitTokens = new ArrayList(tokenizer.countTokens());

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();

            if (trim) {
                token = token.trim();
            }

            splitTokens.add(token);
        }

        return splitTokens;
    }

    /**
     * Convert the provided string to lower case leaving all other as they were
     *
     * @param content
     * @param select
     * @return
     */
    public static final String toLowerSelectedCi(String content, String select) {
        String buffer = "";
        String tl = content.toLowerCase();
        String ls = select.toLowerCase();

        for (int i = 0; i < content.length(); i++) {
            int id = tl.indexOf(ls, i);
            if (id != -1) {
                buffer += content.substring(i, id);

                buffer += ls;
                i = id + 2;
            } else {
                buffer += content.charAt(i);
            }
        }
        return buffer;
    }

    /**
     *
     * @param stringToSplit
     * @param delimitter
     * @param trim
     * @return
     */
    public static final List splitDelimCi(String stringToSplit, String delimitter, boolean trim) {

        stringToSplit = toLowerSelectedCi(stringToSplit, delimitter);

        if (stringToSplit == null) {
            return new ArrayList();
        }

        if (delimitter == null) {
            throw new IllegalArgumentException();
        }

        String ldelim = delimitter.toLowerCase();

        return Arrays.asList(stringToSplit.split(ldelim));
    }

    /**
     *
     * @param stringToSplit
     * @param delimiter
     * @param markers
     * @param markerCloses
     * @param trim
     * @return
     */
    public static final List split(String stringToSplit, String delimiter, String markers, String markerCloses, boolean trim) {
        if (stringToSplit == null) {
            return new ArrayList();
        }

        if (delimiter == null) {
            throw new IllegalArgumentException();
        }

        int delimPos = 0;
        int currentPos = 0;

        List splitTokens = new ArrayList();

        while ((delimPos = indexOfIgnoreCaseRespectMarker(currentPos, stringToSplit, delimiter, markers, markerCloses, false)) != -1) {
            String token = stringToSplit.substring(currentPos, delimPos);

            if (trim) {
                token = token.trim();
            }

            splitTokens.add(token);
            currentPos = delimPos + 1;
        }

        if (currentPos < stringToSplit.length()) {
            String token = stringToSplit.substring(currentPos);

            if (trim) {
                token = token.trim();
            }

            splitTokens.add(token);
        }

        return splitTokens;
    }

    private static boolean startsWith(byte[] dataFrom, String chars) {
        for (int i = 0; i < chars.length(); i++) {
            if (dataFrom[i] != chars.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param searchIn
     * @param startAt
     * @param searchFor
     * @return
     */
    public static boolean startsWithIgnoreCase(String searchIn, int startAt, String searchFor) {
        return searchIn.regionMatches(true, startAt, searchFor, 0, searchFor.length());
    }

    /**
     *
     * @param searchIn
     * @param searchFor
     * @return
     */
    public static boolean startsWithIgnoreCase(String searchIn, String searchFor) {
        return startsWithIgnoreCase(searchIn, 0, searchFor);
    }

    /**
     *
     * @param searchIn
     * @param searchFor
     * @return
     */
    public static boolean startsWithIgnoreCaseAndNonAlphaNumeric(String searchIn, String searchFor) {
        if (searchIn == null) {
            return searchFor == null;
        }

        int beginPos = 0;

        int inLength = searchIn.length();

        for (beginPos = 0; beginPos < inLength; beginPos++) {
            char c = searchIn.charAt(beginPos);

            if (Character.isLetterOrDigit(c)) {
                break;
            }
        }
        return startsWithIgnoreCase(searchIn, beginPos, searchFor);
    }

    /**
     *
     * @param searchIn
     * @param searchFor
     * @return
     */
    public static boolean startsWithIgnoreCaseAndWs(String searchIn, String searchFor) {
        return startsWithIgnoreCaseAndWs(searchIn, searchFor, 0);
    }

    /**
     *
     * @param searchIn
     * @param searchFor
     * @param beginPos
     * @return
     */
    public static boolean startsWithIgnoreCaseAndWs(String searchIn, String searchFor, int beginPos) {
        if (searchIn == null) {
            return searchFor == null;
        }

        int inLength = searchIn.length();

        while ((beginPos < inLength)
                && (Character.isWhitespace(searchIn.charAt(beginPos)))) {
            beginPos++;
        }

        return startsWithIgnoreCase(searchIn, beginPos, searchFor);
    }

    /**
     *
     * @param source
     * @param prefix
     * @param suffix
     * @return
     */
    public static byte[] stripEnclosure(byte[] source, String prefix, String suffix) {
        if ((source.length >= prefix.length() + suffix.length()) && (startsWith(source, prefix)) && (endsWith(source, suffix))) {
            int totalToStrip = prefix.length() + suffix.length();
            int enclosedLength = source.length - totalToStrip;
            byte[] enclosed = new byte[enclosedLength];

            int startPos = prefix.length();
            int numToCopy = enclosed.length;
            System.arraycopy(source, startPos, enclosed, 0, numToCopy);

            return enclosed;
        }
        return source;
    }

    public static String ucFirst(String content) {
        if (content != null) {
            String ch = content.substring(0, 1).toUpperCase();
            return ch + content.substring(1, content.length());
        }
        return content;
    }

    public static String ucWords(String content) {
        String uc = "";
        for (String word : StringUtils.split(content, " ", true)) {
            uc += (ucFirst(word) + " ");
        }
        return uc;
    }

    /**
     *
     * @param str
     * @param ch
     * @return
     */
    public static String trimCharacter(String str, char ch) {
        String nw = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ch) {
                nw += str.charAt(i);
            }
        }
        return nw;
    }

    /**
     *
     * @param buffer
     * @return
     */
    public static final String toAsciiString(byte[] buffer) {
        return toAsciiString(buffer, 0, buffer.length);
    }

    /**
     *
     * @param buffer
     * @param startPos
     * @param length
     * @return
     */
    public static final String toAsciiString(byte[] buffer, int startPos, int length) {
        char[] charArray = new char[length];
        int readpoint = startPos;

        for (int i = 0; i < length; i++) {
            charArray[i] = ((char) buffer[readpoint]);
            readpoint++;
        }

        return new String(charArray);
    }

    /**
     *
     * @param searchIn
     * @param searchForWildcard
     * @return
     */
    public static int wildCompare(String searchIn, String searchForWildcard) {
        if ((searchIn == null) || (searchForWildcard == null)) {
            return -1;
        }

        if (searchForWildcard.equals("%")) {
            return 1;
        }

        int result = -1;

        char wildcardMany = '%';
        char wildcardOne = '_';
        char wildcardEscape = '\\';

        int searchForPos = 0;
        int searchForEnd = searchForWildcard.length();

        int searchInPos = 0;
        int searchInEnd = searchIn.length();

        while (searchForPos != searchForEnd) {
            char wildstrChar = searchForWildcard.charAt(searchForPos);

            while ((searchForWildcard.charAt(searchForPos) != wildcardMany) && (wildstrChar != wildcardOne)) {
                if ((searchForWildcard.charAt(searchForPos) == wildcardEscape) && (searchForPos + 1 != searchForEnd)) {
                    searchForPos++;
                }

                if ((searchInPos == searchInEnd) || (Character.toUpperCase(searchForWildcard.charAt(searchForPos++)) != Character.toUpperCase(searchIn.charAt(searchInPos++)))) {
                    return 1;
                }

                if (searchForPos == searchForEnd) {
                    return searchInPos != searchInEnd ? 1 : 0;
                }

                result = 1;
            }

            if (searchForWildcard.charAt(searchForPos) == wildcardOne) {
                do {
                    if (searchInPos == searchInEnd) {
                        return result;
                    }

                    searchInPos++;

                    searchForPos++;
                } while ((searchForPos < searchForEnd) && (searchForWildcard.charAt(searchForPos) == wildcardOne));

                if (searchForPos == searchForEnd) {
                    break;
                }
            }
            if (searchForWildcard.charAt(searchForPos) == wildcardMany) {
                searchForPos++;

                for (; searchForPos != searchForEnd; searchForPos++) {
                    if (searchForWildcard.charAt(searchForPos) != wildcardMany) {
                        if (searchForWildcard.charAt(searchForPos) != wildcardOne) {
                            break;
                        }
                        if (searchInPos == searchInEnd) {
                            return -1;
                        }

                        searchInPos++;
                    }

                }

                if (searchForPos == searchForEnd) {
                    return 0;
                }

                if (searchInPos == searchInEnd) {
                    return -1;
                }
                char cmp;
                if (((cmp = searchForWildcard.charAt(searchForPos)) == wildcardEscape) && (searchForPos + 1 != searchForEnd)) {
                    cmp = searchForWildcard.charAt(++searchForPos);
                }

                searchForPos++;
                do {
                    while ((searchInPos != searchInEnd) && (Character.toUpperCase(searchIn.charAt(searchInPos)) != Character.toUpperCase(cmp))) {
                        searchInPos++;
                    }
                    if (searchInPos++ == searchInEnd) {
                        return -1;
                    }

                    int tmp = wildCompare(searchIn, searchForWildcard);

                    if (tmp <= 0) {
                        return tmp;
                    }
                } while ((searchInPos != searchInEnd) && (searchForWildcard.charAt(0) != wildcardMany));

                return -1;
            }
        }

        return searchInPos != searchInEnd ? 1 : 0;
    }

    /**
     *
     * @param s
     * @param c
     * @return
     */
    public static int lastIndexOf(byte[] s, char c) {
        if (s == null) {
            return -1;
        }

        for (int i = s.length - 1; i >= 0; i--) {
            if (s[i] == c) {
                return i;
            }
        }

        return -1;
    }

    /**
     *
     * @param s
     * @param c
     * @return
     */
    public static int indexOf(byte[] s, char c) {
        if (s == null) {
            return -1;
        }

        int length = s.length;

        for (int i = 0; i < length; i++) {
            if (s[i] == c) {
                return i;
            }
        }

        return -1;
    }

    /**
     *
     * @param src
     * @param stringOpens
     * @param stringCloses
     * @param slashStarComments
     * @param slashSlashComments
     * @param hashComments
     * @param dashDashComments
     * @return
     */
    public static String stripComments(String src, String stringOpens, String stringCloses, boolean slashStarComments, boolean slashSlashComments, boolean hashComments, boolean dashDashComments) {
        if (src == null) {
            return null;
        }

        StringBuffer buf = new StringBuffer(src.length());

        StringReader sourceReader = new StringReader(src);

        int contextMarker = 0;
        boolean escaped = false;
        int markerTypeFound = -1;

        int ind = 0;

        int currentChar = 0;
        try {
            while ((currentChar = sourceReader.read()) != -1) {
                if ((markerTypeFound != -1) && (currentChar == stringCloses.charAt(markerTypeFound)) && (!escaped)) {
                    contextMarker = 0;
                    markerTypeFound = -1;
                } else if (((ind = stringOpens.indexOf(currentChar)) != -1) && (!escaped) && (contextMarker == 0)) {
                    markerTypeFound = ind;
                    contextMarker = currentChar;
                }

                if ((contextMarker == 0) && (currentChar == 47) && ((slashSlashComments) || (slashStarComments))) {
                    currentChar = sourceReader.read();
                    if ((currentChar == 42) && (slashStarComments)) {
                        int prevChar = 0;

                        while (((currentChar = sourceReader.read()) != 47) || (prevChar != 42)) {
                            if (currentChar == 13) {
                                currentChar = sourceReader.read();
                                if (currentChar == 10) {
                                    currentChar = sourceReader.read();
                                }
                            } else if (currentChar == 10) {
                                currentChar = sourceReader.read();
                            }

                            if (currentChar < 0) {
                                break;
                            }
                            prevChar = currentChar;
                        }
                    }
                    if ((currentChar != 47) || (!slashSlashComments));
                } else {
                    while (((currentChar = sourceReader.read()) != 10) && (currentChar != 13) && (currentChar >= 0)) {
                        if ((contextMarker == 0) && (currentChar == 35) && (hashComments));
                        while (true) {
                            if (((currentChar = sourceReader.read()) != 10) && (currentChar != 13) && (currentChar >= 0)) {
                                if ((contextMarker == 0) && (currentChar == 45) && (dashDashComments)) {
                                    currentChar = sourceReader.read();

                                    if ((currentChar == -1) || (currentChar != 45)) {
                                        buf.append('-');

                                        if (currentChar == -1) {
                                            break;
                                        }
                                        buf.append(currentChar);
                                        break;
                                    }

                                    while (((currentChar = sourceReader.read()) != 10) && (currentChar != 13) && (currentChar >= 0));
                                }
                            }

                        }

                    }

                }

                if (currentChar != -1) {
                    buf.append((char) currentChar);
                }
            }
        } catch (IOException ioEx) {
        }
        return buf.toString();
    }

    static {
        for (int i = -128; i <= 127; i++) {
            allBytes[(i - -128)] = ((byte) i);
        }

        String allBytesString = new String(allBytes, 0, 255);

        int allBytesStringLen = allBytesString.length();

        for (int i = 0;
                (i < 255) && (i < allBytesStringLen); i++) {
            byteToChars[i] = allBytesString.charAt(i);
        }
        try {
            toPlainStringMethod = BigDecimal.class.getMethod("toPlainString", new Class[0]);
        } catch (NoSuchMethodException nsme) {
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Static
    /**
     *
     * @param message
     * @param arguments
     * @return
     */
    public static String format(
            final String message,
            final Object... arguments) {
        return MessageFormat.format(message, arguments);
    }

    /**
     *
     * @param string
     * @return
     */
    public static String leftTrim(
            final String string) {
        return string.replaceFirst(LEFT_WHITESPACE, EMPTY_STRING);
    }

    /**
     *
     * @param string
     * @return
     */
    public static String rightTrim(
            final String string) {
        return string.replaceFirst(RIGHT_WHITESPACE, EMPTY_STRING);
    }

    /**
     *
     * @param string
     * @return
     */
    public static char fetchMnemonic(
            final String string) {
        // source is org.openide.awt.Mnemonics
        int i = findMnemonicAmpersand(string);
        return (i >= 0) ? string.charAt(i + 1) : NO_MNEMONIC;
    }

    /**
     *
     * @param string
     * @return
     */
    public static String stripMnemonic(
            final String string) {
        int i = findMnemonicAmpersand(string);
        String s = string;
        if (i >= 0) {
            if (string.startsWith("<html>")) { // NOI18N
                // Workaround for JDK bug #6510775                
                s = string.substring(0, i)
                        + "<u>" + string.charAt(i + 1) + "</u>" + // NOI18N
                        string.substring(i + 2);
            } else {
                s = string.substring(0, i) + string.substring(i + 1);
            }
        }
        return s;
    }

    // source - org.openide.awt.Mnenonics;
    /**
     *
     * @param text
     * @return
     */
    public static int findMnemonicAmpersand(String text) {
        int i = -1;
        boolean isHTML = text.startsWith("<html>");

        do {
            // searching for the next ampersand
            i = text.indexOf(MNEMONIC_CHAR, i + 1);

            if ((i >= 0) && ((i + 1) < text.length())) {
                if (isHTML) {
                    boolean startsEntity = false;
                    for (int j = i + 1; j < text.length(); j++) {
                        char c = text.charAt(j);
                        if (c == ';') {
                            startsEntity = true;
                            break;
                        }
                        if (!Character.isLetterOrDigit(c)) {
                            break;
                        }
                    }
                    if (!startsEntity) {
                        return i;
                    }
                } else {
                    // before ' '
                    if (text.charAt(i + 1) == ' ') {
                        continue;

                        // before ', and after '
                    } else if ((text.charAt(i + 1) == '\'') && (i > 0) && (text.charAt(i - 1) == '\'')) {
                        continue;
                    }

                    // ampersand is marking mnemonics
                    return i;
                }
            }
        } while (i >= 0);

        return -1;
    }

    /**
     *
     * @param string
     * @return
     */
    public static String capitalizeFirst(
            final String string) {
        return EMPTY_STRING + Character.toUpperCase(string.charAt(0)) + string.substring(1);
    }

    /**
     *
     * @param propertyName
     * @return
     */
    public static String getGetterName(
            final String propertyName) {
        return "get" + capitalizeFirst(propertyName);
    }

    /**
     *
     * @param propertyName
     * @return
     */
    public static String getBooleanGetterName(
            final String propertyName) {
        return "is" + capitalizeFirst(propertyName);
    }

    /**
     *
     * @param propertyName
     * @return
     */
    public static String getSetterName(
            final String propertyName) {
        return "set" + capitalizeFirst(propertyName);
    }

    /**
     *
     * @param string
     * @return
     */
    public static String getFilenameFromUrl(
            final String string) {
        String url = string.trim();

        int index = Math.max(
                url.lastIndexOf(FORWARD_SLASH),
                url.lastIndexOf(BACK_SLASH));
        int length = url.length();
        return (index > 0 && (index < length - 1))
                ? url.substring(index + 1, length) : null;
    }

    /**
     *
     * @param longBytes
     * @return
     */
    public static String formatSize(
            final long longBytes) {
        StringBuffer result = new StringBuffer();

        double bytes = (double) longBytes;

        // try as GB
        double gigabytes = bytes / 1024. / 1024. / 1024.;
        if (gigabytes > 1.) {
            return String.format("%.1f GB", gigabytes);
        }

        // try as MB
        double megabytes = bytes / 1024. / 1024.;
        if (megabytes > 1.) {
            return String.format("%.1f MB", megabytes);
        }

        // try as KB
        double kilobytes = bytes / 1024.;
        if (kilobytes > .5) {
            return String.format("%.1f KB", kilobytes);
        }

        // return as bytes
        return EMPTY_STRING + longBytes + " B";
    }

    /**
     *
     * @param bytes
     * @return
     */
    public static String asHexString(
            final byte[] bytes) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];

            String byteHex = Integer.toHexString(b);
            if (byteHex.length() == 1) {
                byteHex = "0" + byteHex;
            }
            if (byteHex.length() > 2) {
                byteHex = byteHex.substring(byteHex.length() - 2);
            }

            builder.append(byteHex);
        }

        return builder.toString();
    }

    /**
     *
     * @param string
     * @param number
     * @return
     */
    public static String pad(
            final String string,
            final int number) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < number; i++) {
            builder.append(string);
        }

        return builder.toString();
    }

    /**
     *
     * @param string
     * @param escape
     * @return
     */
    public static String escapeSpace(String string, char escape) {
        return escapeCharacter(string, ' ', escape);
    }

    /**
     *
     * @param string
     * @param ch
     * @param escape
     * @return
     */
    public static String escapeCharacter(String string, char ch, char escape) {
        return string.replace(ch, escape);
    }

    /**
     *
     * @param s
     * @return
     */
    public static String escapeSQLString(String s) {
        int length = s.length();
        int newLength = length;
        // first check for characters that might
        // be dangerous and calculate a length
        // of the string that has escapes.
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            switch (c) {
                case '\\':
                case '\"':
                case '\'':
                case '\0': {
                    newLength += 1;
                }
                break;
            }
        }
        if (length == newLength) {
            // nothing to escape in the string
            return s;
        }
        StringBuilder sb = new StringBuilder(newLength);
        for (int i = 0; i < length; i++) {
            char c = s.charAt(i);
            switch (c) {
                case '\\': {
                    sb.append("\\");
                }
                break;
                case '\"': {
                    sb.append("\"");
                }
                break;
                case '\'': {
                    sb.append("''");
                }
                break;
                case '\0': {
                    sb.append("\\0");
                }
                break;
                default: {
                    sb.append(c);
                }
            }
        }
        return sb.toString();
    }

    /**
     *
     * @param string
     * @return
     */
    public static String escapeRegExp(
            final String string) {
        return string.replace(BACK_SLASH, BACK_SLASH + BACK_SLASH).replace("$", "\\$");
    }

    /**
     *
     * @param stream
     * @return
     * @throws IOException
     */
    public static String readStream(
            final InputStream stream) throws IOException {
        return readStream(stream, null);
    }

    /**
     *
     * @param stream
     * @param charset
     * @return
     * @throws IOException
     */
    public static String readStream(
            final InputStream stream, String charset) throws IOException {
        StringBuilder builder = new StringBuilder();

        byte[] buffer = new byte[1024];
        while (stream.available() > 0) {
            int read = stream.read(buffer);

            String readString = (charset == null)
                    ? new String(buffer, 0, read)
                    : new String(buffer, 0, read, charset);
            String[] strings = splitByLines(readString);
            for (int i = 0; i < strings.length; i++) {
                builder.append(strings[i]);
                if (i != strings.length - 1) {
                    builder.append("/");//get line separator
                }
            }
        }

        return builder.toString();
    }

    /**
     *
     * @param date
     * @return
     */
    public static String httpFormat(
            final Date date) {
        return new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US).format(date);
    }

    /**
     *
     * @param clazz
     * @return
     */
    public static String asPath(
            final Class clazz) {
        return clazz.getPackage().getName().replace('.', '/');
    }

    /**
     *
     * @param string
     * @param replacement
     * @param begin
     * @param end
     * @return
     */
    public static String replace(
            final String string,
            final String replacement,
            final int begin,
            final int end) {
        return string.substring(0, begin) + replacement + string.substring(end);
    }

    /**
     * Escapes the path using the platform-specific escape rules.
     *
     * @param path Path to escape.
     * @return Escaped path.
     */
//    public static String escapePath(
//            final String path) {
//        String localPath = path;
//
//        if (localPath.indexOf(' ') > -1) {
//            if (SystemUtils.isWindows()) {
//                localPath = QUOTE + localPath + QUOTE;
//            } else {
//                localPath = localPath.replace(SPACE,
//                        BACK_SLASH + SPACE); //NOI18N
//            }
//        }
//
//        return localPath;
//    }
    /**
     * Joins a command string and its arguments into a single string using the
     * platform-specific rules.
     *
     * @param throwable
     * @return The joined string.
     */
//    public static String joinCommand(
//            final String... commandArray) {
//        StringBuffer command = new StringBuffer();
//
//        for (int i = 0; i < commandArray.length; i++) {
//            command.append(escapePath(commandArray[i]));
//            if (i != commandArray.length - 1) {
//                command.append(SPACE); //NOI18N
//            }
//        }
//
//        return command.toString();
//    }
    // object -> string .////////////////////////////////////////////////////////////
    public static String asString(
            final Throwable throwable) {
        final StringWriter writer = new StringWriter();

        throwable.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

    /**
     *
     * @param objects
     * @return
     */
    public static String asString(
            final List<? extends Object> objects) {
        return asString(objects.toArray(), 0, objects.size(), ", ");
    }

    /**
     *
     * @param objects
     * @param separator
     * @return
     */
    public static String asString(
            final List<? extends Object> objects,
            final String separator) {
        return asString(objects.toArray(), 0, objects.size(), separator);
    }

    /**
     *
     * @param objects
     * @param offset
     * @param length
     * @param separator
     * @return
     */
    public static String asString(
            final List<? extends Object> objects,
            final int offset,
            final int length,
            final String separator) {
        return asString(objects.toArray(), offset, length, separator);
    }

    /**
     *
     * @param objects
     * @return
     */
    public static String asString(
            final Object[] objects) {
        return asString(objects, 0, objects.length, ", ");
    }

    /**
     *
     * @param objects
     * @param separator
     * @return
     */
    public static String asString(
            final Object[] objects,
            final String separator) {
        return asString(objects, 0, objects.length, separator);
    }

    /**
     *
     * @param objects
     * @param offset
     * @param length
     * @param separator
     * @return
     */
    public static String asString(
            final Object[] objects,
            final int offset,
            final int length,
            final String separator) {
        final StringBuilder result = new StringBuilder();

        for (int i = offset; i < offset + length; i++) {
            result.append(EMPTY_STRING + objects[i]);

            if (i != offset + length - 1) {
                result.append(separator);
            }
        }

        return result.toString();
    }

    // base64 ///////////////////////////////////////////////////////////////////////
    /**
     *
     * @param string
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String base64Encode(
            final String string) throws UnsupportedEncodingException {
        return base64Encode(string, ENCODING_UTF8);
    }

    /**
     *
     * @param string
     * @param charset
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String base64Encode(
            final String string,
            final String charset) throws UnsupportedEncodingException {
        final StringBuilder builder = new StringBuilder();
        final byte[] bytes = string.getBytes(charset);

        int i;
        for (i = 0; i < bytes.length - 2; i += 3) {
            int byte1 = bytes[i] & BIN_11111111;
            int byte2 = bytes[i + 1] & BIN_11111111;
            int byte3 = bytes[i + 2] & BIN_11111111;

            builder.append(
                    BASE64_TABLE[byte1 >> 2]);
            builder.append(
                    BASE64_TABLE[((byte1 << 4) & BIN_00110000) | (byte2 >> 4)]);
            builder.append(
                    BASE64_TABLE[((byte2 << 2) & BIN_00111100) | (byte3 >> 6)]);
            builder.append(
                    BASE64_TABLE[byte3 & BIN_00111111]);
        }

        if (i == bytes.length - 2) {
            int byte1 = bytes[i] & BIN_11111111;
            int byte2 = bytes[i + 1] & BIN_11111111;

            builder.append(
                    BASE64_TABLE[byte1 >> 2]);
            builder.append(
                    BASE64_TABLE[((byte1 << 4) & BIN_00110000) | (byte2 >> 4)]);
            builder.append(
                    BASE64_TABLE[(byte2 << 2) & BIN_00111100]);
            builder.append(
                    BASE64_PAD);
        }

        if (i == bytes.length - 1) {
            int byte1 = bytes[i] & BIN_11111111;

            builder.append(
                    BASE64_TABLE[byte1 >> 2]);
            builder.append(
                    BASE64_TABLE[(byte1 << 4) & BIN_00110000]);
            builder.append(
                    BASE64_PAD);
            builder.append(
                    BASE64_PAD);
        }

        return builder.toString();
    }

    /**
     *
     * @param string
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String base64Decode(
            final String string) throws UnsupportedEncodingException {
        return base64Decode(string, ENCODING_UTF8);
    }

    /**
     *
     * @param string
     * @param charset
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String base64Decode(
            final String string,
            final String charset) throws UnsupportedEncodingException {
        int completeBlocksNumber = string.length() / 4;
        int missingBytesNumber = 0;

        if (string.endsWith("=")) {
            completeBlocksNumber--;
            missingBytesNumber++;
        }
        if (string.endsWith("==")) {
            missingBytesNumber++;
        }

        int decodedLength = (completeBlocksNumber * 3) + (3 - missingBytesNumber) % 3;
        byte[] decodedBytes = new byte[decodedLength];

        int encodedCounter = 0;
        int decodedCounter = 0;
        for (int i = 0; i < completeBlocksNumber; i++) {
            int byte1 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];
            int byte2 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];
            int byte3 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];
            int byte4 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];

            decodedBytes[decodedCounter++] = (byte) ((byte1 << 2) | (byte2 >> 4));
            decodedBytes[decodedCounter++] = (byte) ((byte2 << 4) | (byte3 >> 2));
            decodedBytes[decodedCounter++] = (byte) ((byte3 << 6) | byte4);
        }

        if (missingBytesNumber == 1) {
            int byte1 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];
            int byte2 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];
            int byte3 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];

            decodedBytes[decodedCounter++] = (byte) ((byte1 << 2) | (byte2 >> 4));
            decodedBytes[decodedCounter++] = (byte) ((byte2 << 4) | (byte3 >> 2));
        }

        if (missingBytesNumber == 2) {
            int byte1 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];
            int byte2 = BASE64_REVERSE_TABLE[string.charAt(encodedCounter++)];

            decodedBytes[decodedCounter++] = (byte) ((byte1 << 2) | (byte2 >> 4));
        }

        return new String(decodedBytes, charset);
    }

    // normal <-> ascii only ////////////////////////////////////////////////////////
    /**
     *
     * @param string
     * @return
     */
    public static String parseAscii(final String string) {
        final Properties properties = new Properties();

        // we don't really care about enconding here, as the input string is
        // expected to be ASCII-only, which means it's the same for any encoding
        try {
            properties.load(new ByteArrayInputStream(("key=" + string).getBytes()));
        } catch (IOException e) {
//            ErrorManager.notifyWarning(
//                    "Cannot parse string",
//                    e);
            return string;
        }

        return (String) properties.get("key");
    }

    /**
     *
     * @param string
     * @return
     */
    public static String convertToAscii(final String string) {
        final Properties properties = new Properties();

        properties.put("uberkey", string);

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            properties.store(baos, EMPTY_STRING);
        } catch (IOException e) {
            return string;
        }

        final Matcher matcher = Pattern.
                compile("uberkey=(.*)$", Pattern.MULTILINE).
                matcher(baos.toString());

        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return string;
        }
    }

    // string -> object /////////////////////////////////////////////////////////////
    /**
     *
     * @param string
     * @return
     */
    public static List<String> asList(
            final String string) {
        return asList(string, ", ");
    }

    /**
     *
     * @param string
     * @param separator
     * @return
     */
    public static List<String> asList(
            final String string, final String separator) {
        return Arrays.asList(string.split(separator));
    }

    /**
     *
     * @param string
     * @return
     */
    public static Locale parseLocale(
            final String string) {
        final String[] parts = string.split("_");

        switch (parts.length) {
            case 1:
                return new Locale(parts[0]);
            case 2:
                return new Locale(parts[0], parts[1]);
            default:
                return new Locale(parts[0], parts[1], parts[2]);
        }
    }

    /**
     *
     * @param stringsMap
     * @param inLocale
     * @return
     */
    public static String getLocalizedString(final Map<Locale, String> stringsMap, final Locale inLocale) {
        final String message = stringsMap.get(inLocale);
        if (message == null && !inLocale.equals(new Locale(EMPTY_STRING))) {
            final Locale upLocale;
            if (!inLocale.getVariant().equals(EMPTY_STRING)) {
                upLocale = new Locale(inLocale.getLanguage(), inLocale.getCountry());
            } else if (!inLocale.getCountry().equals(EMPTY_STRING)) {
                upLocale = new Locale(inLocale.getLanguage());
            } else {
                upLocale = new Locale(EMPTY_STRING);
            }
            return getLocalizedString(stringsMap, upLocale);
        } else {
            return message;
        }
    }

    /**
     *
     * @param string
     * @return
     */
    public static URL parseUrl(
            final String string) {
        try {
            return new URL(string);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Constants
    /**
     *
     */
    public static final String BACK_SLASH
            = "\\"; // NOI18N

    /**
     *
     */
    public static final String FORWARD_SLASH
            = "/"; // NOI18N

    /**
     *
     */
    public static final String DOUBLE_BACK_SLASH
            = "\\\\"; // NOI18N

    /**
     *
     */
    public static final String ENCODING_UTF8
            = "UTF-8"; // NOI18N

    /**
     *
     */
    public static final String CR = "\r"; // NOI18N

    /**
     *
     */
    public static final String LF = "\n"; // NOI18N

    /**
     *
     */
    public static final String DOT = "."; // NOI18N

    /**
     *
     */
    public static final String EMPTY_STRING = ""; // NOI18N

    /**
     *
     */
    public static final String CRLF = CR + LF;

    /**
     *
     */
    public static final String CRLFCRLF = CRLF + CRLF;

    /**
     *
     */
    public static final String SPACE = " "; // NOI18N

    /**
     *
     */
    public static final String QUOTE = "\""; // NOI18N

    /**
     *
     */
    public static final String EQUAL = "="; // NOI18N

    /**
     *
     */
    public static final String UNDERSCORE = "_"; // NOI18N

    /**
     *
     */
    public static final String NEW_LINE_PATTERN = "(?:\r\n|\n|\r)"; // NOI18N

    private static final String LEFT_WHITESPACE = "^\\s+"; // NOI18N
    private static final String RIGHT_WHITESPACE = "\\s+$"; // NOI18N

    private static final char MNEMONIC_CHAR = '&';
    private static final String MNEMONIC = "&"; // NOI18N
    private static final char NO_MNEMONIC = '\u0000';

    private static final char[] BASE64_TABLE = new char[]{
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
        'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
        'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
        'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
        'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', '+', '/'
    };

    private static final byte[] BASE64_REVERSE_TABLE = new byte[]{
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, 62, -1, -1, -1, 63, 52, 53,
        54, 55, 56, 57, 58, 59, 60, 61, -1, -1,
        -1, -1, -1, -1, -1, 0, 1, 2, 3, 4,
        5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
        25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
        29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
        39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
        49, 50, 51
    };

    private static final char BASE64_PAD = '=';

    private static final int BIN_11111111 = 0xff;
    private static final int BIN_00110000 = 0x30;
    private static final int BIN_00111100 = 0x3c;
    private static final int BIN_00111111 = 0x3f;

    /**
     *
     */
    public static final String ERROR_CANNOT_PARSE_STATUS
            = "StrU.error.cannot.parse.status";//NOI18N

    /**
     *
     */
    public static final String ERROR_UNKNOWN_PLATFORM
            = "StrU.error.unknown.platform";//NOI18N
}
