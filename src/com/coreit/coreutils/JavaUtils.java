/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils;

import com.coreit.coreutils.logging.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Ramadan
 */
public class JavaUtils {

    public static boolean saveObject(File file, Object object) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fis = new FileOutputStream(file);
            try (ObjectOutputStream oos = new ObjectOutputStream(fis)) {
                oos.writeObject(object);
            }
            return true;
        } catch (IOException ex) {
        }

        return false;
    }

    public static boolean saveObject(String path, Object object) {
        return saveObject(new File(path), object);
    }

    public static Object readObject(File file) {
        FileInputStream fis;
        ObjectInputStream ois;
        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            return ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Console.log(ex);
        }
        return null;
    }

    public static Object readObject(String path) {
        return readObject(new File(path));
    }
}
