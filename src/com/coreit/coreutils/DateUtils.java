/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 */
package com.coreit.coreutils;

import com.coreit.coreutils.logging.Console;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author Ramadan
 */
public final class DateUtils {

    public static String getCompactTimestamp() {
        return COMPACT_TIMESTAMP.format(new Date());
    }

    public static String getFormattedTimestamp() {
        return DETAILED_TIMESTAMP.format(new Date());
    }

    public static Long getCurrentTimestamp() {
        return new Date().getTime();
    }

    private DateUtils() {
        // does nothing
    }

    public static final DateFormat COMPACT_TIMESTAMP = new SimpleDateFormat("yyyyMMddHHmmss"); // NOI18N

    public static final DateFormat DETAILED_TIMESTAMP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); // NOI18N
    public static final String YdMdD_Hms = "yyyy-MM-dd HH:mm:ss";
    public static final String YdMdD = "yyyy-MM-dd";
    public static final String DdMdY = "dd-MM-yyyy";

    private static final Map<String, String> DATE_FORMAT_REGEXPS = new HashMap<String, String>() {
        {
            put("^\\d{8}$", "yyyyMMdd");
            put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
            put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
            put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
            put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
            put("^\\d{12}$", "yyyyMMddHHmm");
            put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
            put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm");
            put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm");
            put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "MM/dd/yyyy HH:mm");
            put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm");
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm");
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm");
            put("^\\d{14}$", "yyyyMMddHHmmss");
            put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
            put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss");
            put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}.\\d{1,2}$", "dd-MM-yyyy HH:mm:ss.SSS");
            put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss");
            put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}.\\d{1,2}$", "yyyy-MM-dd HH:mm:ss.SSS");
            put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "MM/dd/yyyy HH:mm:ss");
            put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}.\\d{1,2}$", "MM/dd/yyyy HH:mm:ss.SSS");
            put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss");
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss");
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss");
        }
    };

    /**
     * Determine SimpleDateFormat pattern matching with the given date string.
     * Returns null if format is unknown. You can simply extend DateUtil with
     * more formats if needed.
     *
     * @param dateString The date string to determine the SimpleDateFormat
     * pattern for.
     * @return The matching SimpleDateFormat pattern, or null if format is
     * unknown.
     * @see SimpleDateFormat
     */
    public static String determineDateFormat(String dateString) {
        for (String regexp : DATE_FORMAT_REGEXPS.keySet()) {
            if (dateString.toLowerCase().matches(regexp)) {
                return DATE_FORMAT_REGEXPS.get(regexp);
            }
        }
        return null; // Unknown format.
    }

    public static Date getDate(String date) {
        String format = determineDateFormat(date);
        if (format != null) {
            try {
                DateFormat df = new SimpleDateFormat(format);
                return df.parse(date);
            } catch (ParseException ex) {

            }
        }
        return null;
    }

    public static void main(String... args) {
        Console.log(determineDateFormat("2015-09-28 10:00:08.5"));
    }

    public static int compareDate(String first, String second) throws Exception {
        Date fd = getDate(first);
        Date sd = getDate(second);
        try {
            return fd.compareTo(sd);
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public static String formatDate(Date date, String format) {
        DateFormat writeFormat = new SimpleDateFormat(format);
        String formattedDate = writeFormat.format(date);
        return formattedDate;
    }

    public static Date now() {
        return new Date();
    }

    public static Date nowPlus(long amount) {
        Date d = now();
        long timestamp = d.getTime();
        timestamp += amount;
        d.setTime(timestamp);
        return d;
    }

    public static Date DateAdd(Date date, long amount) {
        long timestamp = date.getTime();
        date.setTime(timestamp + amount);
        return date;
    }

    public static int dateDiff(Date first, Date second) {
        long diff = first.getTime() - second.getTime();
        return (int) diff / (24 * 60 * 60 * 1000);
    }

    public static int yearDiff(Date first, Date second) {
        Calendar fd = Calendar.getInstance();
        fd.setTime(first);
        Calendar sc = Calendar.getInstance();
        sc.setTime(second);
        return fd.get(Calendar.YEAR) - sc.get(Calendar.YEAR);
    }

    public static int hourDiff(Date first, Date second) {
        Calendar fd = Calendar.getInstance();
        fd.setTime(first);
        Calendar sc = Calendar.getInstance();
        sc.setTime(second);
        return fd.get(Calendar.HOUR) - sc.get(Calendar.HOUR);
    }

    public static int minuteDiff(Date first, Date second) {
        Calendar fd = Calendar.getInstance();
        fd.setTime(first);
        Calendar sc = Calendar.getInstance();
        sc.setTime(second);
        return fd.get(Calendar.MINUTE) - sc.get(Calendar.MINUTE);
    }

    public static int secondDiff(Date first, Date second) {
        Calendar fd = Calendar.getInstance();
        fd.setTime(first);
        Calendar sc = Calendar.getInstance();
        sc.setTime(second);
        return fd.get(Calendar.SECOND) - sc.get(Calendar.SECOND);
    }

    public static Date parse(String date, String format) {
        DateFormat dformat = new SimpleDateFormat(format, Locale.ENGLISH);
        Date ddate;
        try {
            ddate = dformat.parse(date);
        } catch (ParseException ex) {
            Console.log(ex);
            return null;
        }
        return ddate;
    }

}
