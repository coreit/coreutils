/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.ui;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;

/**
 *
 * @author Ramadan
 */
public class UiUtils {

    /**
     * Returns true if the mouse event specifies the left mouse button.
     *
     * @param anEvent a MouseEvent object
     * @return true if the left mouse button was active
     */
    public static boolean isLeftMouseButton(MouseEvent anEvent) {
        return ((anEvent.getModifiers() & InputEvent.BUTTON1_MASK) != 0);
    }

    /**
     * Returns true if the mouse event specifies the middle mouse button.
     *
     * @param anEvent a MouseEvent object
     * @return true if the middle mouse button was active
     */
    public static boolean isMiddleMouseButton(MouseEvent anEvent) {
        return ((anEvent.getModifiers() & InputEvent.BUTTON2_MASK) == InputEvent.BUTTON2_MASK);
    }

    /**
     * Returns true if the mouse event specifies the right mouse button.
     *
     * @param anEvent a MouseEvent object
     * @return true if the right mouse button was active
     */
    public static boolean isRightMouseButton(MouseEvent anEvent) {
        return ((anEvent.getModifiers() & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK);
    }

    public static boolean isDoubleClick(MouseEvent anEvent) {
        return isLeftMouseButton(anEvent) && anEvent.getClickCount() == 2;
    }
}
