/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.ui;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JDialog;

/**
 *
 * @author Ramadan
 */
public abstract class FreeDialog extends JDialog implements WindowListener {

    public FreeDialog(java.awt.Frame parent) {
        super(parent, false);
        addWindowListener(this);
    }

    @Override
    public void setModal(boolean modal) {
        super.setModal(false);
    }

    public void showDialog() {
        setVisible(true);
    }

    public void positive() {
        setVisible(false);
        dispose();
    }

    public void negative() {
        setVisible(false);
        dispose();
    }

    protected abstract void onClose();

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        onClose();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}
