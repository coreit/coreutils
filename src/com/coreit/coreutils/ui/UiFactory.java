/*
 * Copyright 2015 Ramadan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.coreit.coreutils.ui;

import com.coreit.coreutils.logging.Console;
import com.coreit.coreutils.ui.components.TimeoutDialog;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Ramadan
 */
public class UiFactory {

    /**
     * Set Application look and feel of specific system
     */
    public static void setLF() {
        try {
            // Set System L&F
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException |
                ClassNotFoundException |
                InstantiationException |
                IllegalAccessException e) {
            Console.log(e);
        }
    }

    public static Image getImage(String path, Class clazz) {
        if (path != null) {
            try {
                return ImageIO.read(clazz.getClassLoader().getResource(path));
            } catch (IOException ex) {
                Console.log(ex);
                return null;
            }
        }
        return null;
    }

    public static Image getImage(String path) {
        return getImage(path, UiFactory.class);
    }

    public static void cleanTabBorders() {
        UIManager.getDefaults().put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));
        UIManager.getDefaults().put("TabbedPane.tabAreaInsets", new Insets(-1, 0, 0, 0));
        //UIManager.getDefaults().put("TabbedPane.contentAreaColor",new Color(240,240,240));
        //UIManager.getDefaults().put("TabbedPane.focus", new Color(240, 240, 240, 0));
        //UIManager.getDefaults().put("TabbedPane.tabAreaBackground", new Color(240, 240, 240));
    }

    public static File openFileSave(String title, String ext, String extDesc, String selectedFile, Window parent) {
        JFileChooser ch = new JFileChooser();
        ch.setDialogTitle(title);
        ch.setSelectedFile(new File(selectedFile));
        if (ext != null && extDesc != null) {
            FileFilter filter = new FileNameExtensionFilter(extDesc, ext);

            ch.setFileFilter(filter);
        }
        int result = ch.showSaveDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            return ch.getSelectedFile();
        }
        return null;
    }

    public static File openFileSave(String title, String ext, String extDesc, Window parent) {
        JFileChooser ch = new JFileChooser();
        ch.setDialogTitle(title);
        if (ext != null && extDesc != null) {
            FileFilter filter = new FileNameExtensionFilter(extDesc, ext);

            ch.setFileFilter(filter);
        }
        int result = ch.showSaveDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            return ch.getSelectedFile();
        }
        return null;
    }

    //public JpopUpMenu
    public static File openFileSave(Map<String, String> filters, Window parent) {
        JFileChooser ch = new JFileChooser();
        String strExt = null;
        String desc = null;
        if (filters != null) {
            strExt = "";
            desc = "";
            for (String ext : filters.keySet()) {
                strExt += ext + " & ";
                desc += filters.get(ext) + ",";
            }
            FileFilter filter = new FileNameExtensionFilter(desc, strExt);
            ch.setFileFilter(filter);
        }
        return openFileSave("Save", strExt, desc, parent);
    }

    public static File openFileChooser(int selectionMode, Window parent) {
        return openFileChooser(selectionMode, null, parent);
    }

    public static File openFileChooser(int selectionMode, String extension, String description, Window parent) {
        Map map = new HashMap();
        map.put(extension, description);
        return openFileChooser(selectionMode, map, parent);
    }

    public static File openFileChooser(String title, String ext, String desc, Window parent) {
        JFileChooser ch = new JFileChooser();
        ch.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (ext != null && desc != null) {
            FileFilter filter = new FileNameExtensionFilter(desc, ext);
            ch.setFileFilter(filter);
        }
        int result = ch.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            return ch.getSelectedFile();
        }
        return null;
    }

    public static File openFileChooser(int selectionMode, Map<String, String> filters, Window parent) {
        JFileChooser ch = new JFileChooser();
        ch.setFileSelectionMode(selectionMode);
        if (filters != null) {
            String strExt = "";
            String desc = "";
            for (String ext : filters.keySet()) {
                strExt += ext + " & ";
                desc += filters.get(ext) + ",";
            }
            FileFilter filter = new FileNameExtensionFilter(desc, strExt);
            ch.setFileFilter(filter);
        }
        int result = ch.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            return ch.getSelectedFile();
        }
        return null;
    }

    /**
     *
     * @param window
     * @param imagePath
     */
    public static void setWindowIcon(Window window, String imagePath) {
        ClassLoader loader = window.getClass().getClassLoader();
        ImageIcon icon;
        try {
            icon = new ImageIcon(loader.getResource(imagePath));
        } catch (NullPointerException ex) {
            icon = UiFactory.getImageIcon(imagePath);
        }
        window.setIconImage(icon.getImage());
    }

    /**
     *
     * @param message message to display
     * @return
     */
    public static int showErrorDialog(String message) {
        return JOptionPane.showConfirmDialog(null, message,
                "Error Occured",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     *
     * @param title the title of the dialog
     * @param message message to display
     * @return
     */
    public static String showTextInputDialog(String title, String message) {
        return JOptionPane.showInputDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     *
     * @param title the title of the dialog
     * @param message message to display
     */
    public static void showInfoDialog(String title, String message) {
        JOptionPane.showConfirmDialog(null, message, title, JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
    }

    
    public static int showTimeoutDialog(Frame parent,String title,String message,long timeout){
        TimeoutDialog diag = new TimeoutDialog(parent,title,message,timeout);
        return diag.showDialog();
    }
    /**
     *
     * @param message message to display
     */
    public static void showInfoDialog(String message) {
        JOptionPane.showConfirmDialog(null, message, "Information", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
    }

    public static int showYesNoDialog(String title, String message, Component parent) {
        return JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    }

    public static int showYesNoDialog(String title, String message) {
        return showYesNoDialog(title, message, null);
    }

    public static int showYesNoDialog(String message) {
        return showYesNoDialog("Confirm", message, null);
    }

    /**
     *
     * @param message message to display
     * @return the value of the selected key
     */
    public static int showConfirmDialog(String message) {
        return JOptionPane.showConfirmDialog(null, message, "Confirm", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
    }

    public static void setBusy(final Component component, final boolean busy) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (busy) {
                    component.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                } else {
                    component.setCursor(Cursor.getDefaultCursor());
                }
            }
        });

    }

    /**
     *
     * @param title
     * @param message
     * @return
     */
    public static int showConfirmDialog(String title, String message) {
        return JOptionPane.showConfirmDialog(null, message, title,
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);
    }

    /**
     *
     * @param parent
     * @param message
     * @return
     */
    public static int showConfirmDialog(Component parent, String message) {
        return JOptionPane.showConfirmDialog(parent, message, "Confirm",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);
    }

    public static String showInputDialog(String title, inputPane pane) {
        int ret = JOptionPane.showConfirmDialog(null, pane, title,
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE);
        if (ret == JOptionPane.OK_OPTION) {
            return pane.getInput();
        }
        return null;
    }

//    public static String showTextInputDialog(String title,String message){
//        
//    }
    public static ImageIcon getImageIcon(Class clazz, String path) {
        try {
            if (clazz != null) {
                return new ImageIcon(ImageIO.read(clazz.getClassLoader().getResource(path)));
            } else {
                return new ImageIcon(ImageIO.read(UiFactory.class.getClassLoader().getResource(path)));
            }
        } catch (IOException ex) {
            return null;
        }
    }

    public static ImageIcon getImageIcon(String path) {
        return getImageIcon(null, path);
    }

    public static void showOkMessageDialog(String title, String message) {
        JOptionPane.showConfirmDialog(null, message, title, JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
    }

    public static int showYesNoMessageDialog(String header, String message) {
        return JOptionPane.showConfirmDialog(null, message, header, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    }

    public static void showErrorDialog(String header, String message) {
        JOptionPane.showConfirmDialog(null, message, header, JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);

    }

    public interface inputPane {

        public String getInput();
    }
}
