/*
 * Copyright 2015 CoreIT.
 *
 * Licensed under the CoreIt Private License, Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * The source code in this file is CoreIT PROPRIETARY/CONFIDENTIAL,
 * All rights reserved,
 */
package com.coreit.coreutils.ui.components;

import com.coreit.coreutils.logging.Console;
import com.coreit.coreutils.ui.UiUtils;
import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.TabbedPaneUI;

public class ClosableTabbedPane extends JTabbedPane implements ChangeListener {

    private TabCloseUI closeUi;

    public interface TabListener {

        public void tabClosing(String parent, String tabName);

        public void tabSelected(String parent, String tabName);
    }

    private List<TabListener> listeners;

    public ClosableTabbedPane() {
        setBorder(new MatteBorder(1, 0, 0, 0, new Color(204, 204, 204)));
        listeners = new ArrayList();
        setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        postInit();
    }

    private void postInit() {
        addChangeListener(this);
        //closeUi = new TabCloseUI(this);
    }

    public void addListener(TabListener listener) {
        listeners.add(listener);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        try {
            for (TabListener li : listeners) {
                li.tabSelected(getName(), getSelectedComponent().getName());
            }
        } catch (NullPointerException ex) {

        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //closeUi.paint(g);
    }

    @Override
    public void addTab(String title, Component component) {
        component.setName(title);
        super.addTab(title , component);
    }

    public String getTabTitleAt(int index) {
        return super.getTitleAt(index).trim();
    }

    @Override
    public void setUI(TabbedPaneUI ui) {
        //super.setUI(new CloseUi());
        super.setUI(new ClosableWinUi());
    }

    private class TabCloseUI implements MouseListener, MouseMotionListener {

        private ClosableTabbedPane tabbedPane;
        private int closeX = 0, closeY = 0, meX = 0, meY = 0;
        private int selectedTab;
        private final int width = 6, height = 6;
        private final Rectangle rectangle = new Rectangle(0, 0, width, height);
        private JPopupMenu menu;

        private TabCloseUI() {

        }

        public TabCloseUI(ClosableTabbedPane pane) {
            tabbedPane = pane;
            tabbedPane.addMouseMotionListener(this);
            tabbedPane.addMouseListener(this);
            this.postInit();
        }

        private void postInit() {
            menu = new JPopupMenu();
            JMenuItem close = new JMenuItem("Close");
            JMenuItem closeAll = new JMenuItem("Close All");
            JMenuItem closeOther = new JMenuItem("Close Other");
            menu.add(close);
            menu.add(closeAll);
            menu.add(closeOther);
        }

        @Override
        public void mouseEntered(MouseEvent me) {
        }

        @Override
        public void mouseExited(MouseEvent me) {
        }

        @Override
        public void mousePressed(MouseEvent me) {
        }

        @Override
        public void mouseClicked(MouseEvent me) {
            if (UiUtils.isRightMouseButton(me) && mouseUnderClose(me.getX(), me.getY())) {
                menu.show(ClosableTabbedPane.this, me.getX(), me.getY());
            }
        }

        @Override
        public void mouseDragged(MouseEvent me) {
        }

        @Override
        public void mouseReleased(MouseEvent me) {
//            if (mouseUnderClose(me.getX(), me.getY()) && UiUtils.isLeftMouseButton(me)) {
//                boolean isToCloseTab = tabAboutToClose(selectedTab);
//                if (isToCloseTab && selectedTab > -1) {
//                    for (TabListener li : listeners) {
//                        li.tabClosing(ClosableTabbedPane.this.getName(), tabbedPane.getTitleAt(selectedTab).trim());
//                    }
//                    tabbedPane.removeTabAt(selectedTab);
//                }
//                selectedTab = tabbedPane.getSelectedIndex();
//            }
        }

        @Override
        public void mouseMoved(MouseEvent me) {
            meX = me.getX();
            meY = me.getY();
            if (mouseOverTab(meX, meY)) {
                controlCursor();
                tabbedPane.repaint();
            }
        }

        private void controlCursor() {
            if (tabbedPane.getTabCount() > 0) {
                if (mouseUnderClose(meX, meY)) {
                    tabbedPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
                    if (selectedTab > -1) {
                        tabbedPane.setToolTipTextAt(selectedTab, "Close " + tabbedPane.getTitleAt(selectedTab));
                    }
                } else {
                    tabbedPane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    if (selectedTab > -1) {
                        //tabbedPane.setToolTipTextAt(selectedTab, "");
                    }
                }
            }
        }

        private boolean mouseUnderClose(int x, int y) {
            rectangle.x = closeX;
            rectangle.y = closeY;
            return rectangle.contains(x, y);
        }

        public void paint(Graphics g) {

            int tabCount = tabbedPane.getTabCount();
            for (int j = 0; j < tabCount; j++) {
                if (tabbedPane.getComponent(j).isShowing()) {
                    int x = tabbedPane.getBoundsAt(j).x + tabbedPane.getBoundsAt(j).width - width - 5;
                    int y = tabbedPane.getBoundsAt(j).y + 7;
                    drawClose(g, x, y);
                    break;
                }
            }
            if (mouseOverTab(meX, meY)) {
                drawClose(g, closeX, closeY);
            }
        }

        private void drawClose(Graphics g, int x, int y) {
            if (tabbedPane != null && tabbedPane.getTabCount() > 0) {
                Graphics2D g2 = (Graphics2D) g;

                Image img1 = Toolkit.getDefaultToolkit().getImage("close.png");
                boolean bo = g2.drawImage(img1, x, y, ClosableTabbedPane.this);
                Console.log(bo);
                g2.finalize();
                //drawColored(g2, isUnderMouse(x, y) ? Color.RED : Color.WHITE, x, y);
            }
        }

        private void drawColored(Graphics2D g2, Color color, int x, int y) {
            g2.setStroke(new BasicStroke(3, BasicStroke.JOIN_ROUND, BasicStroke.CAP_ROUND));
            g2.setColor(Color.GRAY);
            g2.drawLine(x, y, x + width, y + height);
            g2.drawLine(x + width, y, x, y + height);
            g2.setColor(color);
            g2.setStroke(new BasicStroke(1, BasicStroke.JOIN_ROUND, BasicStroke.CAP_ROUND));
            g2.drawLine(x, y, x + width, y + height);
            g2.drawLine(x + width, y, x, y + height);
        }

        private boolean isUnderMouse(int x, int y) {
            return Math.abs(x - meX) < width && Math.abs(y - meY) < height;
        }

        private boolean mouseOverTab(int x, int y) {
            int tabCount = tabbedPane.getTabCount();
            for (int j = 0; j < tabCount; j++) {
                if (tabbedPane.getBoundsAt(j).contains(meX, meY)) {
                    selectedTab = j;
                    closeX = tabbedPane.getBoundsAt(j).x + tabbedPane.getBoundsAt(j).width - width - 5;
                    closeY = tabbedPane.getBoundsAt(j).y + 7;
                    return true;
                }
            }
            return false;
        }

    }

    public void tabAboutToClose(int tabIndex) {
        for (TabListener li : listeners) {
            li.tabClosing(ClosableTabbedPane.this.getName(), getTitleAt(tabIndex).trim());
        }
        removeTabAt(tabIndex);
    }

}
