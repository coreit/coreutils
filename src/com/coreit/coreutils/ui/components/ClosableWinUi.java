/*
 * Copyright (c) 2015, Ramadan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.coreit.coreutils.ui.components;

import com.coreit.coreutils.ui.UiFactory;
import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.SwingConstants;
import static javax.swing.SwingConstants.EAST;
import static javax.swing.SwingConstants.SOUTH;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.text.View;

/**
 *
 * @author Ramadan
 */
public class ClosableWinUi extends WindowsTabbedPaneUI implements MouseListener, MouseMotionListener {

    private final BufferedImage idleX;

    private final BufferedImage overX;

    private int overTabIndex = -1;
    private static final int INACTIVE = 0;
    volatile int closeIndexStatus = INACTIVE;
    private static final int OVER = 1;

    private static final int PRESSED = 2;
    int BUTTONSIZE = 16;
    int WIDTHDELTA = 5;

    public ClosableWinUi() {
        super();

        idleX = (BufferedImage) UiFactory.getImage("idle_close.png");
        overX = (BufferedImage) UiFactory.getImage("over_close.png");
    }

    @Override
    protected void installListeners() {
        super.installListeners();
        tabPane.addMouseListener(this);
        tabPane.addMouseMotionListener(this);
    }

    private int getTabAtLocation(int x, int y) {
        //ensureCurrentLayout();

        int tabCount = tabPane.getTabCount();
        for (int i = 0; i < tabCount; i++) {
            if (rects[i].contains(x, y)) {
                return i;
            }
        }
        return -1;
    }

    protected void updateOverTab(int x, int y) {
        if (overTabIndex != (overTabIndex = getTabAtLocation(x, y))) {
            tabPane.repaint();
        }

    }

    @Override
    protected int calculateTabWidth(int tabPlacement, int tabIndex,
            FontMetrics metrics) {
        int delta = 2 + BUTTONSIZE + WIDTHDELTA;
        return super.calculateTabWidth(tabPlacement, tabIndex, metrics) + delta;
    }

    @Override
    protected void paintFocusIndicator(Graphics g, int tabPlacement,
            Rectangle[] rects, int tabIndex,
            Rectangle iconRect, Rectangle textRect,
            boolean isSelected) {
        Rectangle tabRect = rects[tabIndex];
        if (tabPane.hasFocus() && isSelected) {
            g.setColor(new Color(204, 255, 255, 60));
            g.fillRect(tabRect.x, tabRect.y, tabRect.width, tabRect.height);
        }
    }

    @Override
    protected void layoutLabel(int tabPlacement, FontMetrics metrics,
            int tabIndex, String title, Icon icon, Rectangle tabRect,
            Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        textRect.x = textRect.y = iconRect.x = iconRect.y = 0;

        View v = getTextViewForTab(tabIndex);
        if (v != null) {
            tabPane.putClientProperty("html", v);
        }

        SwingUtilities.layoutCompoundLabel((JComponent) tabPane, metrics,
                title, icon, SwingUtilities.CENTER, SwingUtilities.LEFT,
                SwingUtilities.CENTER, SwingUtilities.CENTER, tabRect,
                iconRect, textRect, textIconGap);

        tabPane.putClientProperty("html", null);

        iconRect.x = tabRect.x + 8;
        textRect.x = iconRect.x + iconRect.width + textIconGap;
    }

    @Override
    protected void paintTab(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect) {
        Rectangle tabRect = rects[tabIndex];
        int selectedIndex = tabPane.getSelectedIndex();
        boolean isSelected = selectedIndex == tabIndex;
        boolean isOver = overTabIndex == tabIndex;
        Graphics2D g2 = (Graphics2D) g;
        super.paintTab(g, tabPlacement, rects, tabIndex, iconRect, textRect);
        if (isOver || isSelected) {
            int dx = tabRect.x + tabRect.width - BUTTONSIZE - WIDTHDELTA;
            int dy = (tabRect.y + tabRect.height) / 2 - (idleX.getHeight() / 2);
            paintCloseIcon(g2, dx, dy, isOver);
        }
    }

    protected class ScrollableTabButton extends BasicArrowButton implements
            UIResource, SwingConstants {

        public ScrollableTabButton(int direction) {
            super(direction, UIManager.getColor("TabbedPane.selected"),
                    UIManager.getColor("TabbedPane.shadow"), UIManager
                    .getColor("TabbedPane.darkShadow"), new Color(204, 204, 204));

        }

        public boolean scrollsForward() {
            return direction == EAST || direction == SOUTH;
        }

    }

//    @Override
//    protected JButton createScrollButton(int direction) {
//        return new ScrollableTabButton(direction);
//    }

    private void controlCursor(boolean isOver) {
        if (isOver) {
            tabPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
            tabPane.setToolTipTextAt(tabPane.getSelectedIndex(), "Close " + tabPane.getTitleAt(tabPane.getSelectedIndex()));
        } else {
            tabPane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }

    }

    protected Rectangle newCloseRect(Rectangle rect) {
        int dx = rect.x + rect.width;
        int dy = (rect.y + rect.height) / 2 - (idleX.getHeight() / 2);
        return new Rectangle(dx - BUTTONSIZE - WIDTHDELTA, dy, BUTTONSIZE,
                BUTTONSIZE);
    }

    protected void updateCloseIcon(int x, int y) {

        if (overTabIndex != -1) {
            int newCloseIndexStatus = INACTIVE;
//
            Rectangle closeRect = newCloseRect(rects[overTabIndex]);
            if (closeRect.contains(x, y)) {
                newCloseIndexStatus = mousePressed ? PRESSED : OVER;
            }
//
            if (closeIndexStatus != (closeIndexStatus = newCloseIndexStatus)) {
                tabPane.repaint();
            }
        }
    }

    private void setTabIcons(int x, int y) {
        //if the mouse isPressed
        if (!mousePressed) {
            updateOverTab(x, y);
        }

        updateCloseIcon(x, y);
    }

    protected void paintCloseIcon(Graphics g, int dx, int dy, boolean isOver) {
        boolean isActive = (closeIndexStatus == PRESSED || closeIndexStatus == OVER) && isOver;
        if (isActive) {
            g.drawImage(overX, dx, dy, null);
        } else {
            g.drawImage(idleX, dx, dy, null);
        }
        controlCursor(isOver);
    }

    @Override
    protected int calculateTabHeight(int tabPlacement, int tabIndex,
            int fontHeight) {

        return super.calculateTabHeight(tabPlacement, tabIndex, fontHeight) + 2;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    boolean mousePressed = false;

    @Override
    public void mousePressed(MouseEvent e) {
        mousePressed = true;
        setTabIcons(e.getX(), e.getY());

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        updateOverTab(e.getX(), e.getY());
        if (closeIndexStatus == PRESSED) {
            closeIndexStatus = OVER;
            tabPane.repaint();
            ((ClosableTabbedPane) tabPane).tabAboutToClose(overTabIndex);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (!mousePressed) {
            overTabIndex = -1;
            tabPane.repaint();
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mousePressed = true;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mousePressed = false;
        setTabIcons(e.getX(), e.getY());
    }

}
