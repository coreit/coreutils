/*
 * Copyright (c) 2015, Ramadan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.coreit.coreutils.ui.components;

import com.coreit.coreutils.logging.Console;
import com.coreit.coreutils.ui.UiFactory;
import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.JComponent;
import static javax.swing.SwingConstants.BOTTOM;
import static javax.swing.SwingConstants.LEFT;
import static javax.swing.SwingConstants.RIGHT;
import static javax.swing.SwingConstants.TOP;

/**
 *
 * @author Ramadan
 */
public class CloseUi extends WindowsTabbedPaneUI {

    private final BufferedImage idleX;

    private final BufferedImage overX;
    private int overTabIndex = -1;
    protected static final int BUTTONSIZE = 15;

    protected static final int WIDTHDELTA = 5;
    private int tabCount;

    public CloseUi() {
        super();
        idleX = (BufferedImage) UiFactory.getImage("idle_close.png");
        overX = (BufferedImage) UiFactory.getImage("over_close.png");
    }

    private void ensureCurrentLayout() {
        if (!tabPane.isValid()) {
            tabPane.validate();
        }
        /*
         * If tabPane doesn't have a peer yet, the validate() call will silently
         * fail. We handle that by forcing a layout if tabPane is still invalid.
         * See bug 4237677.
         */
        if (!tabPane.isValid()) {
            TabbedPaneLayout layout = (TabbedPaneLayout) tabPane.getLayout();
            layout.calculateLayoutInfo();
        }
    }

    //@Override
    public void painto(Graphics g, JComponent c) {
        int tc = tabPane.getTabCount();

        if (tabCount != tc) {
            tabCount = tc;
            //updateMnemonics();
        }

        int selectedIndex = tabPane.getSelectedIndex();
        int tabPlacement = tabPane.getTabPlacement();

        ensureCurrentLayout();

        // Paint content border
        paintContentBorder(g, tabPlacement, selectedIndex);

    }
    @Override
    protected void paintTabBackground(Graphics g, int tabPlacement,
            int tabIndex,
            int x, int y, int w, int h,
            boolean isSelected) {
        g.setColor(isSelected
                ? tabPane.getBackgroundAt(tabIndex) : new Color(204, 204, 204));
        switch (tabPlacement) {
            case LEFT:
                g.fillRect(x + 1, y + 1, w - 1, h - 3);
                break;
            case RIGHT:
                g.fillRect(x, y + 1, w - 2, h - 3);
                break;
            case BOTTOM:
                g.fillRect(x + 1, y, w - 3, h - 1);
                break;
            case TOP:
            default:
                g.fillRect(x + 1, y + 1, w - 3, h - 1);
        }
    }
    @Override
    protected void paintTab(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect) {
        Rectangle tabRect = rects[tabIndex];
        int selectedIndex = tabPane.getSelectedIndex();
        boolean isSelected = selectedIndex == tabIndex;
        boolean isOver = overTabIndex == tabIndex;
        Graphics2D g2 = null;
        if (g instanceof Graphics2D) {
            g2 = (Graphics2D) g;
        }
        String title = tabPane.getTitleAt(tabIndex);
        Font font = tabPane.getFont();
        FontMetrics metrics = g.getFontMetrics(font);
        Icon icon = getIconForTab(tabIndex);

        paintTabBackground(g, tabPlacement, tabIndex, tabRect.x, tabRect.y,
                tabRect.width, tabRect.height, isSelected);

        paintTabBorder(g, tabPlacement, tabIndex, tabRect.x, tabRect.y,
                tabRect.width, tabRect.height, isSelected);

        layoutLabel(tabPlacement, metrics, tabIndex, title, icon, tabRect,
                iconRect, textRect, isSelected);
        paintText(g, tabPlacement, font, metrics, tabIndex, title, textRect,
                isSelected);

        paintIcon(g, tabPlacement, tabIndex, icon, iconRect, isSelected);
        if (isOver || isSelected) {

            int dx = tabRect.x + tabRect.width - BUTTONSIZE - WIDTHDELTA;
            int dy = (tabRect.y + tabRect.height) / 2 - 6;

            paintCloseIcon(g2, dx, dy, isOver);

        }
    }

    private void controlCursor(boolean isOver) {
        if (isOver) {
            tabPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
            tabPane.setToolTipTextAt(tabPane.getSelectedIndex(), "Close " + tabPane.getTitleAt(tabPane.getSelectedIndex()));
        } else {
            tabPane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }

    }

    protected void paintCloseIcon(Graphics g, int dx, int dy, boolean isOver) {
       
        if (isOver) {
            g.drawImage(overX, dx, dy - 1, null);
        } else {
            g.drawImage(idleX, dx, dy - 1, null);
        }
        controlCursor(isOver);
    }

    @Override
    protected int calculateTabWidth(int tabPlacement, int tabIndex,
            FontMetrics metrics) {
        int delta = 2 + BUTTONSIZE + WIDTHDELTA;
        return super.calculateTabWidth(tabPlacement, tabIndex, metrics) + delta;
    }

    @Override
    protected int calculateTabHeight(int tabPlacement, int tabIndex,
            int fontHeight) {

        return super.calculateTabHeight(tabPlacement, tabIndex, fontHeight) + 2;
    }

    private static final int INACTIVE = 0;

    private static final int OVER = 1;

    private static final int PRESSED = 2;
    private int closeIndexStatus = INACTIVE;

    private int maxIndexStatus = INACTIVE;

    private boolean mousePressed = false;

    class MyMouseHandler extends MouseHandler {

        public MyMouseHandler() {
            super();
        }

        public void mousePressed(MouseEvent e) {
            if (closeIndexStatus == OVER) {
                closeIndexStatus = PRESSED;
                tabPane.repaint();
                return;
            }

            if (maxIndexStatus == OVER) {
                maxIndexStatus = PRESSED;
                tabPane.repaint();
                return;
            }

        }

        public void mouseClicked(MouseEvent e) {
            super.mousePressed(e);
            if (e.getClickCount() > 1 && overTabIndex != -1) {
                //((CloseAndMaxTabbedPane) tabPane).fireDoubleClickTabEvent(e, overTabIndex);
            }
        }

        public void mouseReleased(MouseEvent e) {

            updateOverTab(e.getX(), e.getY());

            if (overTabIndex == -1) {
                if (e.isPopupTrigger()) //((CloseAndMaxTabbedPane) tabPane)
                //		.firePopupOutsideTabEvent(e);
                {
                    return;
                }
            }

            //if (isOneActionButtonEnabled() && e.isPopupTrigger()) {
                super.mousePressed(e);

                closeIndexStatus = INACTIVE; //Prevent undesired action when
                maxIndexStatus = INACTIVE; //right-clicking on icons

                //actionPopupMenu.show(tabPane, e.getX(), e.getY());
                //return;
            //}

            if (closeIndexStatus == PRESSED) {
                closeIndexStatus = OVER;
                tabPane.repaint();
                //((CloseAndMaxTabbedPane) tabPane).fireCloseTabEvent(e, overTabIndex);
                return;
            }

            if (maxIndexStatus == PRESSED) {
                maxIndexStatus = OVER;
                tabPane.repaint();
                //((CloseAndMaxTabbedPane) tabPane).fireMaxTabEvent(e, overTabIndex);
                return;
            }

        }

        public void mouseExited(MouseEvent e) {
            if (!mousePressed) {
                overTabIndex = -1;
                tabPane.repaint();
            }
        }

    }

    private int getTabAtLocation(int x, int y) {
        ensureCurrentLayout();

        int count = tabPane.getTabCount();
        for (int i = 0; i < count; i++) {
            if (rects[i].contains(x, y)) {
                return i;
            }
        }
        return -1;
    }

    protected void updateOverTab(int x, int y) {
        if (overTabIndex != (overTabIndex = getTabAtLocation(x, y))) {
            tabPane.repaint();
        }

    }

    protected Rectangle newCloseRect(Rectangle rect) {
        int dx = rect.x + rect.width;
        int dy = (rect.y + rect.height) / 2 - 6;
        return new Rectangle(dx - BUTTONSIZE - WIDTHDELTA, dy, BUTTONSIZE,
                BUTTONSIZE);
    }

    protected Rectangle newMaxRect(Rectangle rect) {
        int dx = rect.x + rect.width;
        int dy = (rect.y + rect.height) / 2 - 6;
        //if (isCloseButtonEnabled) {
        dx -= BUTTONSIZE;
        // }

        return new Rectangle(dx - BUTTONSIZE - WIDTHDELTA, dy, BUTTONSIZE,
                BUTTONSIZE);
    }

    protected void updateCloseIcon(int x, int y) {

        if (overTabIndex != -1) {
            int newCloseIndexStatus = INACTIVE;

            Rectangle closeRect = newCloseRect(rects[overTabIndex]);
            if (closeRect.contains(x, y)) {
                newCloseIndexStatus = mousePressed ? PRESSED : OVER;
            }

            if (closeIndexStatus != (closeIndexStatus = newCloseIndexStatus)) {
                tabPane.repaint();
            }
        }
    }

    private void setTabIcons(int x, int y) {
        //if the mouse isPressed
        if (!mousePressed) {
            updateOverTab(x, y);
        }

        updateCloseIcon(x, y);

    }

    class MyMouseMotionListener implements MouseMotionListener {

        public void mouseMoved(MouseEvent e) {
//            if (actionPopupMenu.isVisible()) {
//                return; //No updates when popup is visible
//            }
            mousePressed = false;
            setTabIcons(e.getX(), e.getY());
        }

        public void mouseDragged(MouseEvent e) {
//            if (actionPopupMenu.isVisible()) {
//                return; //No updates when popup is visible
//            }
            mousePressed = true;
            setTabIcons(e.getX(), e.getY());
        }
    }

}
