/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.ui.components;

import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

public class CustomTreeNode extends DefaultMutableTreeNode {

    /**
     * The icon which is displayed on the JTree object. open, close, leaf icon.
     */
    private ImageIcon icon;

    public CustomTreeNode(ImageIcon icon) {
        this.icon = icon;
    }

    public CustomTreeNode(ImageIcon icon, Object userObject) {
        super(userObject);
        this.icon = icon;
    }

    public CustomTreeNode(Object object) {
        super(object);
    }

    public void onClick(MouseEvent e) {

    }

    public CustomTreeNode(ImageIcon icon, Object userObject, boolean allowsChildren) {
        super(userObject, allowsChildren);
        this.icon = icon;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    public void setIcon(ImageIcon icon) {
        this.icon = icon;
    }
}
