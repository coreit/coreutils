/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

/**
 *
 * @author Ramadan
 */
public class ClosableTabPanel extends JPanel {

    public ClosableTabPanel() {
        setBorder(new MatteBorder(1, 0, 0, 0, new Color(204, 204, 204)));
    }

    public ClosableTabPanel(Component panel,String title){
        this(panel);
        setName(title);
    }
    public ClosableTabPanel(Component panel) {
        this();
        setLayout(new BorderLayout());
        add(panel, BorderLayout.CENTER);
    }
}
