/*
 * Copyright (c) 2015, Ramadan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.coreit.coreutils.ui.components;

import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author Ramadan
 */
public class CustomTree extends JTree implements MouseListener {

    private CustomTreeNode rootNode;
    private DefaultTreeModel customModel;
    private final Toolkit toolkit = Toolkit.getDefaultToolkit();

    public CustomTree(CustomTreeNode root) {
        super(root);
        this.rootNode = root;
        customModel = new DefaultTreeModel(rootNode);
        setCellRenderer(new CustomTreeRenderer());
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        setShowsRootHandles(true);
        addMouseListener(this);
    }

    public CustomTree(Object root) {
        this(new CustomTreeNode(root));
    }

    public CustomTree() {
        this(new CustomTreeNode("Root"));
    }

    @Override
    public TreeModel getModel() {
        return customModel;
    }

    public void setRootNode(Object node, ImageIcon icon) {
        if (icon != null) {
            rootNode = new CustomTreeNode(icon, node);
        } else {
            rootNode = new CustomTreeNode(node);
        }

        customModel.setRoot(rootNode);
    }

    /**
     * Remove all nodes except the root node.
     */
    public void clear() {
        getRootNode().removeAllChildren();
        getCustomModel().reload();
    }

    /**
     * Remove the currently selected node.
     */
    public void removeCurrentNode() {
        TreePath currentSelection = getSelectionPath();
        if (currentSelection != null) {
            DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) (currentSelection.getLastPathComponent());
            MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
            if (parent != null) {
                getCustomModel().removeNodeFromParent(currentNode);
                return;
            }
        }

        // Either there was no selection, or the root was selected.
        toolkit.beep();
    }

    /**
     * Add child to the currently selected node.
     *
     * @param child
     * @return
     */
    public CustomTreeNode addObject(Object child) {
        CustomTreeNode parentNode;
        TreePath parentPath = getSelectionPath();

        if (parentPath == null) {
            parentNode = getRootNode();
        } else {
            parentNode = (CustomTreeNode) (parentPath.getLastPathComponent());
        }

        return addObject(parentNode, child, true);
    }

    public CustomTreeNode addRootChild(Object child) {
        return addRootChild(child, null);
    }

    public void addRootChild(CustomTreeNode node) {
        rootNode.add(node);
    }

    public CustomTreeNode addRootChild(Object child, ImageIcon icon) {
        CustomTreeNode node;
        if (icon != null) {
            node = new CustomTreeNode(icon, child);
        } else {
            node = new CustomTreeNode(child);
        }
        addRootChild(node);
        return node;
    }

    public CustomTreeNode addObject(CustomTreeNode parent, Object child) {
        return addObject(parent, child, false);
    }

    public CustomTreeNode addObject(CustomTreeNode parent, CustomTreeNode childNode, boolean shouldBeVisible) {

        if (parent == null) {
            parent = getRootNode();
        }

        //It is key to invoke this on the TreeModel, and NOT DefaultMutableTreeNode
        getCustomModel().insertNodeInto(childNode, parent, parent.getChildCount());

        //Make sure the user can see the lovely new node.
        if (shouldBeVisible) {
            scrollPathToVisible(new TreePath(childNode.getPath()));
        }
        return childNode;
    }

    public CustomTreeNode addObject(CustomTreeNode parent, Object child, boolean shouldBeVisible) {
        CustomTreeNode childNode = new CustomTreeNode(child);

        if (parent == null) {
            parent = getRootNode();
        }

        //It is key to invoke this on the TreeModel, and NOT DefaultMutableTreeNode
        getCustomModel().insertNodeInto(childNode, parent, parent.getChildCount());

        //Make sure the user can see the lovely new node.
        if (shouldBeVisible) {
            scrollPathToVisible(new TreePath(childNode.getPath()));
        }
        return childNode;
    }

    /**
     * @return the rootNode
     */
    public CustomTreeNode getRootNode() {
        return rootNode;
    }

    /**
     * @param rootNode the rootNode to set
     */
    public void setRootNode(CustomTreeNode rootNode) {
        this.rootNode = rootNode;
    }

    /**
     * @return the customModel
     */
    public DefaultTreeModel getCustomModel() {
        return customModel;
    }

    /**
     * @param customModel the customModel to set
     */
    public void setCustomModel(DefaultTreeModel customModel) {
        this.customModel = customModel;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        TreePath path = getPathForLocation(e.getX(), e.getY());
        Rectangle pathBounds = getUI().getPathBounds(this, path);
        if (pathBounds != null && pathBounds.contains(e.getX(), e.getY())) {
            CustomTreeNode selectedNode = (CustomTreeNode) path.getLastPathComponent();
            setSelectionPath(path);
            selectedNode.onClick(e);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    class MyTreeModelListener implements TreeModelListener {

        @Override
        public void treeNodesChanged(TreeModelEvent e) {
            DefaultMutableTreeNode node;
            node = (DefaultMutableTreeNode) (e.getTreePath().getLastPathComponent());

            /*
             * If the event lists children, then the changed
             * node is the child of the node we've already
             * gotten.  Otherwise, the changed node and the
             * specified node are the same.
             */
            int index = e.getChildIndices()[0];
            node = (DefaultMutableTreeNode) (node.getChildAt(index));

            System.out.println("The user has finished editing the node.");
            System.out.println("New value: " + node.getUserObject());
        }

        public void treeNodesInserted(TreeModelEvent e) {
        }

        public void treeNodesRemoved(TreeModelEvent e) {
        }

        public void treeStructureChanged(TreeModelEvent e) {
        }
    }
}
