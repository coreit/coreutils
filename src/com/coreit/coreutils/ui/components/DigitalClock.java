/*
 * Copyright (c) 2015, Ramadan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.coreit.coreutils.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 *
 * @author Ramadan
 */
public class DigitalClock extends JLabel {

    private String stringTime;
    private int length = 11;
    private boolean autoSize = false;
    private Font font;
    private boolean center = true;
    private boolean showDate = true;

    public DigitalClock() {
        font = new Font("SansSerif", Font.PLAIN, getLength());
        this.setCursor(new Cursor(Cursor.HAND_CURSOR));
        Timer t1 = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        });
        t1.start();
    }

    public void setStringTime(String xyz) {
        this.stringTime = xyz;
    }

    public int findMinimumBetweenTwoNumbers(int a, int b) {
        return (a <= b) ? a : b;
    }

    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        int second = now.get(Calendar.SECOND);

        String correctionHour = (hour < 10) ? "0" : "";
        String correctionMinute = (minute < 10) ? "0" : "";
        String correctionSecond = (second < 10) ? "0" : "";

        setStringTime(correctionHour + hour + ":" + correctionMinute + minute + ":" + correctionSecond + second);
        if (isShowDate()) {
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String date = format.format(new Date());
            setStringTime(getStringTime() + " " + date);
        }
        g.setColor(Color.BLACK);
        if (isAutoSize()) {
            setLength(Math.min(this.getWidth(), this.getHeight()) / 5);
            font = new Font("SansSerif", Font.PLAIN, getLength());
        }
        g.setFont(font);
        if (isCenter()) {
            Graphics2D g2d = (Graphics2D) g;
            FontMetrics fm = g2d.getFontMetrics();
            Rectangle2D r = fm.getStringBounds(getStringTime(), g2d);
            int x = (this.getWidth() - (int) r.getWidth()) / 2;
            int y = this.getHeight() / 2;
            g.drawString(getStringTime(), x, y);
        } else {
            g.drawString(stringTime, (int) length / 6, length / 2);
        }
    }

    public static void main(String... args) {
        JFrame f = new JFrame();
        f.setLayout(new BorderLayout());
        f.add(new DigitalClock(), BorderLayout.CENTER);
        f.setVisible(true);
    }

    /**
     * @return the stringTime
     */
    public String getStringTime() {
        return stringTime;
    }

    /**
     * @return the length
     */
    public final int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the autoSize
     */
    public boolean isAutoSize() {
        return autoSize;
    }

    /**
     * @param autoSize the autoSize to set
     */
    public void setAutoSize(boolean autoSize) {
        this.autoSize = autoSize;
    }

    /**
     * @return the font
     */
    public Font getFont() {
        return font;
    }

    /**
     * @param font the font to set
     */
    public void setFont(Font font) {
        this.font = font;
    }

    /**
     * @return the center
     */
    public boolean isCenter() {
        return center;
    }

    /**
     * @param center the center to set
     */
    public void setCenter(boolean center) {
        this.center = center;
    }

    /**
     * @return the showDate
     */
    public boolean isShowDate() {
        return showDate;
    }

    /**
     * @param showDate the showDate to set
     */
    public void setShowDate(boolean showDate) {
        this.showDate = showDate;
    }
}
