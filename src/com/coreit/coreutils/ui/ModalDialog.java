/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.ui;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author Ramadan
 */
public abstract class ModalDialog extends JDialog implements WindowListener {

    private int exitValue = JOptionPane.CLOSED_OPTION;

    public ModalDialog(Frame parent) {
        super(parent, true);
        addWindowListener(this);
    }

    public ModalDialog() {
        setModal(true);
    }

    public int showDialog() {
        setVisible(true);
        return getExitValue();
    }

    public abstract Object getData();

    public void positive() {
        this.exitValue = JOptionPane.OK_OPTION;
        setVisible(false);
        dispose();
    }

    public void negative() {
        this.exitValue = JOptionPane.CANCEL_OPTION;
        setVisible(false);
        dispose();
    }

    @Override
    public final void setModal(boolean modal) {
        super.setModal(true);
    }

    /**
     * @return the exitValue
     */
    public int getExitValue() {
        return exitValue;
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        exitValue = JOptionPane.CLOSED_OPTION;
        setVisible(false);
        dispose();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}
