/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Ramadan
 */
public class CollectionUtils {

    /**
     *
     * @param list1
     * @param list2
     * @return
     */
    public static boolean intersects(final List<? extends Object> list1, final List<? extends Object> list2) {
        for (int i = 0; i < list1.size(); i++) {
            for (int j = 0; j < list2.size(); j++) {
                if (list1.get(i).equals(list2.get(j))) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * @param <T>
     * @param list1
     * @param list2
     * @return
     */
    public static <T> List<T> intersect(final List<? extends T> list1, final List<? extends T> list2) {
        final List<T> intersection = new LinkedList<>();

        for (T item : list1) {
            if (list2.contains(item)) {
                intersection.add(item);
            }
        }

        return intersection;
    }

    /**
     *
     * @param <T>
     * @param list1
     * @param list2
     * @return
     */
    public static <T> List<T> substract(final List<? extends T> list1, final List<? extends T> list2) {
        final List<T> result = new LinkedList<>();

        for (T item1 : list1) {
            boolean found = false;

            for (T item2 : list2) {
                if (item1.equals(item2)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                result.add(item1);
            }
        }

        return result;
    }

    /**
     *
     * @param collection
     * @param object
     * @return
     */
    public static boolean contains(String[] collection, Object object) {
        if (collection == null) {
            return false;
        }
        for (Object col : collection) {
            if (col.equals(object)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param collection
     * @param object
     * @return
     */
    public static boolean contains(Collection collection, Object object) {
        for (Object col : collection) {
            if (col.equals(object)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param <T>
     * @param list
     * @return
     */
    public static <T> LinkedList<T> reverse(LinkedList<? extends T> list) {
        LinkedList li = new LinkedList();
        Iterator it = list.descendingIterator();
        while (it.hasNext()) {
            li.add(it.next());
        }
        return li;
    }
}
