package com.coreit.coreutils;

import com.coreit.coreutils.TimeUtils.countdownListener;
import com.coreit.coreutils.logging.Console;
import com.coreit.coreutils.persistence.FileUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.List;
import java.util.Timer;

/**
 *
 * @author Ramadan
 */
public class ProcessWatcher extends Thread {

    private final String process;
    private boolean autoStart = false;
    private String path;
    private processListener watcher;
    private boolean adminProcess = false;

    private volatile boolean startAllowed = true;
    private Timer startTimer;
    /**
     * The interval between process checks
     */
    public static int CHECK_INTERVAL = 30000;
    private static boolean starting = false;
    private static String WORK_DIR = "/";

    /**
     * @return true if is auto staring
     */
    public boolean isAutoStart() {
        return autoStart;
    }

    /**
     * @param autoStart the autoStart to set
     */
    public void setAutoStart(boolean autoStart) {
        this.autoStart = autoStart;
    }

    /**
     *
     * @param dir
     */
    public static void setWorkDirectory(String dir) {
        WORK_DIR = dir;
    }

    /**
     *
     * @return
     */
    public static String getWorkDirectory() {
        return WORK_DIR;
    }

    /**
     * @return the path
     */
    public String getPath() {
        if (path == null) {
            return process;
        }
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the adminProcess
     */
    public boolean isAdminProcess() {
        return adminProcess;
    }

    /**
     *
     */
    public void stopWatch() {
        watcher = null;
        if (startTimer != null) {
            startTimer.cancel();
        }
        interrupt();
    }

    /**
     * @param adminProcess the adminProcess to set
     */
    public void setAdminProcess(boolean adminProcess) {
        this.adminProcess = adminProcess;
    }

    /**
     * Process Listener
     */
    public interface processListener {

        /**
         *
         */
        public void processEnded();

        /**
         *
         */
        public void processRunning();
    }

    /**
     *
     * @param listener a listener interface which get notified when process
     * changes occur
     * @param process the process to watch
     */
    public ProcessWatcher(processListener listener, String process) {
        this.process = process;
        this.watcher = listener;
        setName("ProcessWatcher:" + process);
    }

    /**
     * Check if the process assigned to this object is running
     *
     * @return true if process is running
     */
    public boolean isRunning() {
        return isRunning(process);
    }

    /**
     * Check if the process {@code process} is running. for window system a
     * process called {@code wmic.exe} is spawned and filtered this will create
     * a .bat file called WmicTempBat.bat file in the current user temporary
     * directory
     *
     * @param process the name of the process to check
     * @return true if the process is running.
     */
    public static boolean isRunning(String process) {
        String line;
        if (SystemUtils.isWindows()) {
            try {
                Process proc = Runtime.getRuntime().exec(new String[]{"wmic.exe"}, null, SystemUtils.getTempDirectory());
                BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                OutputStreamWriter oStream = new OutputStreamWriter(proc.getOutputStream());
                oStream.write("process where name='" + process + "'");
                oStream.flush();
                oStream.close();
                int lines = 0;
                while ((line = input.readLine()) != null) {
                    if (!line.isEmpty()) {
                        lines++;
                    }
                    if (lines >= 3) {
                        List li = StringUtils.split(line, " ", true);
                        if (!li.isEmpty()) {
                            String pro = li.get(0).toString();
                            return pro.compareToIgnoreCase(process) == 0;
                        }
                    }
                }
                input.close();
            } catch (IOException ex) {
                Console.log(ex);
            }
        }
        return false;
    }

    /**
     * Start a process with Administrative privileges
     *
     * @param name The name of the process to start
     * @return true if succeeded
     */
    public static boolean startAdminProcess(String name) {
        if (starting) {
            return false;
        }
        try {
            starting = true;
            ClassLoader loader = ProcessWatcher.class.getClassLoader();
            URL resource = loader.getResource("elevator.bat");
            File file = File.createTempFile("elevator", ".bat");
            file.deleteOnExit();
            FileUtils.copyFile(resource.openStream(), FileUtils.openOutputStream(file));
            String path = file.getAbsolutePath();
           
            Process p = Runtime.getRuntime().exec(new String[]{"cmd", "/c", path, name}, null, new File(WORK_DIR));
            starting = false;
//            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
//            String ss;
//            Console.log(stdError.readLine());
//            while ((ss = stdError.readLine()) != null) {
//                Console.log(ss);
//            }

            return true;
        } catch (IOException ex) {
            Console.log(ex);
            starting = false;
            return false;
        }
    }

    /**
     * Start a process normally
     *
     * @param name
     * @return
     */
    public static boolean startProcess(String name) {
        if (starting) {
            return false;
        }
        try {
            starting = true;
            Runtime.getRuntime().exec(name);
            starting = false;
            return true;
        } catch (IOException ex) {
            starting = false;
            return false;
        }
    }

    @Override
    public void run() {
        while (true) {
            if (!isRunning()) {
                if (watcher != null) {
                    watcher.processEnded();
                }
                if (isAutoStart() && startAllowed) {
                    if (isAdminProcess()) {
                        startAdminProcess(getPath());
                    } else {
                        startProcess(getPath());
                    }
                    startAllowed = false;
                    startTimer = TimeUtils.countDown(new countdownListener() {

                        @Override
                        public void countEnd() {
                            startAllowed = true;
                        }

                    }, 60, 1000); //prompt start every 60 secs
                }
            } else {
                if (watcher != null) {
                    watcher.processRunning();
                }
            }
            try {
                sleep(CHECK_INTERVAL);
            } catch (InterruptedException ex) {
                Console.log(ex);
                return;
            }
        }
    }
}
