/*
 * Copyright 2014 Ramadan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.coreit.coreutils.logging;

import com.coreit.coreutils.DateUtils;
import java.awt.Color;
import java.awt.Component;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author Ramadan
 * @since 1.0.0
 */
public class Console {

    /**
     * Represent color BLACK
     */
    public static final int BLACK = 30;

    /**
     * Represent color RED
     */
    public static final int RED = 31;

    /**
     * Represent color GREEN
     */
    public static final int GREEN = 32;

    /**
     * Represent color YELLOW
     */
    public static final int YELLOW = 33;

    /**
     * Represent color BLUE
     */
    public static final int BLUE = 34;

    /**
     * Represent color MAGENTA
     */
    public static final int MAGENTA = 35;

    /**
     * Represent color CYAN
     */
    public static final int CYAN = 36;

    /**
     * Represent color WHITE
     */
    public static final int WHITE = 37;
    
    private static final int BLACK_BACKGROUND = 40;
    private static final int RED_BACKGROUND = 41;
    private static final int GREEN_BACKGROUND = 42;
    private static final int YELLOW_BACKGROUND = 43;
    private static final int BLUE_BACKGROUND = 44;
    private static final int MAGENTA_BACKGROUND = 45;
    private static final int WHITE_BACKGROUND = 30;
    
    private static boolean enabled = true;
    
    private static int level = 0;
    
    private static Component _screen;
    private static boolean _scroll = false;
    private static boolean toFile = false;
    private static File file;
    private static BufferedWriter out;
    private static boolean enableTime = true;
    private static String filePath = "log.txt";

    /**
     * Enable file logging
     *
     * @param enable
     */
    public static void enableFileLog(boolean enable) {
        toFile = enable;
        if (enable && file == null) {
            try {
                file = new File(filePath);
                if (!file.exists()) {
                    file.createNewFile();
                }
            } catch (IOException ex) {
                Console.log(ex);
                toFile = false;
            }
        }
    }
    
    public static boolean isEnableFile() {
        return toFile;
    }
    
    public static void echo(String content) {
        System.out.print(content);
    }

    private static void write(String txt) {
        String msg = "";
        if (isEnableTime()) {
            msg = "[" + DateUtils.getFormattedTimestamp() + "]: ";
        }
        msg += txt;
        System.out.println(msg);
        if (toFile) {
            try {
                if (out == null) {
                    if (!file.exists()) {
                        toFile = file.createNewFile();
                    }
                    out = new BufferedWriter(new FileWriter(file, true));
                }
                out.append(msg).append("\r\n");
                out.flush();
            } catch (IOException ex) {
                ex.printStackTrace(); //we cant console log here
                out = null;
            }
        }
    }
    
    public static String getFilePath() {
        return filePath;
    }
    
    public static void clearLogs() {
        if (toFile) {
            try {
                if (out == null) {
                    if (file.exists()) {
                        FileWriter fileOut;
                        fileOut = new FileWriter(file, false);
                        fileOut.write("");
                        fileOut.flush();
                        fileOut.close();
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Console.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void setFilePath(String path) {
        filePath = path;
    }

    /**
     *
     * @param msg message to be logged
     */
    public static void log(Object msg) {
        if (isEnabled()) {
            write(msg.toString());
        }
    }

    /**
     * Forces the Console to write to output even if it has been disabled.
     *
     * @param msg the message to write
     */
    public static void forceWrite(Object msg) {
        write(msg.toString());
        
    }

    /**
     *
     * @param colorTag
     * @param msg message to be logged
     */
    public static void log(int colorTag, Object msg) {
        if (isEnabled()) {
            write((char) 27 + "[" + colorTag + ";40m" + msg.toString());
        }
    }

    /**
     * log a message object and the exception thrown
     *
     * @param msg
     * @param ex
     */
    public static void log(Object msg, Exception ex) {
        if (isEnabled()) {
            write(msg + ":" + ex.getMessage());
        }
    }

    /**
     *
     * @param msg
     * @param ex
     * @param tag
     */
    public static void log(Object msg, Exception ex, String tag) {
        if (isEnabled()) {
            write(tag + msg + ex.getMessage());
        }
    }

    /**
     *
     * @param msg
     * @param tag
     */
    public static void log(Object msg, String tag) {
        if (isEnabled()) {
            write(tag + ":" + msg.toString());
        }
    }

    /**
     *
     * @param msg
     * @param tag
     */
    public static void forceWrite(Object msg, String tag) {
        String string = "";
        if (Console.isEnableTime()) {
            string = "[" + DateUtils.getFormattedTimestamp() + "]:";
        }
        string += "'" + tag + "'"
                + msg;
        System.out.println(string);
        
    }

    /**
     *
     * @param ex
     */
    public static void logError(Exception ex) {
        switch (getLevel()) {
            case 0:
                logError(ex.toString());
                break;
            case 1:
                Logger.getLogger(Console.class.getName()).log(Level.WARNING, null, ex);
                break;
            case 3:
                Logger.getLogger(Console.class.getName()).log(Level.SEVERE, null, ex);
                break;
            default:
                logError(ex.toString());
                break;
        }
    }

    /**
     *
     * @param msg
     */
    public static void logError(Object msg) {
        if (isEnabled()) {
            String string = "";
            if (isEnableTime()) {
                string = "[" + DateUtils.getFormattedTimestamp() + "]: ";
            }
            string += (char) 27 + "[" + RED + ";40m" + msg;
            System.err.println(string);
        }
    }

    /**
     *
     * @param msg
     * @param ex
     */
    public static void logError(Object msg, Exception ex) {
        logError(msg.toString() + ":" + ex.getMessage());
    }

    /**
     * Set the output screen for component logging, you will have to set this
     * component in order to enable component logging, the preferred component
     * is {@code JTextArea}
     *
     * @param screen a swing component to use as a screen output for the console
     */
    public static void setScreen(Component screen) {
        _screen = screen;
    }

    /**
     * Enables or disables auto scrolling when writing to component.
     *
     * @param scroll
     */
    public static void setAutoScroll(boolean scroll) {
        _scroll = scroll;
    }

    /**
     *
     * @param message
     */
    public static void writeToComponent(String message) {
        writeToComponent(message, 0);
    }

    /**
     *
     * @param message
     * @param type
     */
    public static void writeToComponent(String message, int type) {
        if (_screen != null) {
            if (_screen instanceof JTextArea) {
                JTextArea ta = (JTextArea) _screen;
                if (type == 0) {
                    ta.setForeground(Color.black);
                } else {
                    ta.setForeground(Color.red);
                }
                if (isEnableTime()) {
                    ta.append("[" + DateUtils.getFormattedTimestamp() + "]: " + message + "\n");
                }
                if (_scroll) {
                    ta.setCaretPosition(ta.getDocument().getLength());
                }
            }
        }
        log(message);
    }

    /**
     *
     * @param message
     */
    public static void writeErrorToComponent(String message) {
        writeToComponent(message, 1);
    }

    /**
     *
     * @param ex
     */
    public static void writeErrorToComponent(Exception ex) {
        writeToComponent(ex.getMessage(), 1);
    }

    /**
     * @return the enabled
     */
    public static boolean isEnabled() {
        return enabled;
    }

    /**
     * A global method to enable or disable console logging
     *
     * @param enable the enabled to set
     */
    public static void setEnabled(boolean enable) {
        enabled = enable;
    }

    /**
     * The logging level of the console
     *
     * @return the level
     */
    public static int getLevel() {
        return level;
    }

    /**
     * Set the logging level of the console
     *
     * @param level the level to set
     */
    public static void setLevel(int level) {
        Console.level = level;
    }

    /**
     * @return the enableTime
     */
    public static boolean isEnableTime() {
        return enableTime;
    }

    /**
     * @param aEnableTime the enableTime to set
     */
    public static void setEnableTime(boolean aEnableTime) {
        enableTime = aEnableTime;
    }
}
