/*
 * Copyright 2014 Ramadan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.coreit.coreutils.security;

import com.coreit.coreutils.logging.Console;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

/**
 *
 * @author Ramadan
 */
public class Security {

    //private boolean use_salt = false;
    private static ALGORITHMS algol = ALGORITHMS.SHA1;

    public enum ALGORITHMS {

        MD5,
        SHA1,
        SHA256,
        SHA384,
        SHA512,
        BCRYPT
    }

    public static String getSalt() {
        //Always use a SecureRandom generator
        byte[] salt = null;
        SecureRandom sr;
        try {
            sr = SecureRandom.getInstance("SHA1PRNG", "SUN");

            //Create array for salt
            salt = new byte[16];
            //Get a random salt
            sr.nextBytes(salt);
            //return salt
        } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {
            Console.log(ex);
        }
        return salt.toString();
    }

    public static void setDefaultAlgol(ALGORITHMS algo) {
        algol = algo;
    }

    public static String getPassword(String pwd) {
        return hashString(algol, pwd);
    }

    public static String hashString(ALGORITHMS algo, String passwordToHash) {
        String generatedPassword = null;
        String strAlgo = null;
        switch (algol) {
            case MD5:
                strAlgo = "MD5";
                break;
            case SHA1:
                strAlgo = "SHA-1";
                break;
            case SHA256:
                strAlgo = "SHA-256";
                break;
            case SHA384:
                strAlgo = "SHA-384";
                break;
            case SHA512:
                strAlgo = "SHA-512";
                break;
        }

        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance(strAlgo);
            //Add password bytes to digest
            //md.update(salt.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest(passwordToHash.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Console.log(e);
        }

        return generatedPassword;
    }

    public static String getMd5Hash(String string) {
        return hashString(ALGORITHMS.MD5, string);
    }

    public static String getSha1Hash(String string) {
        return hashString(ALGORITHMS.SHA1, string);
    }

}
