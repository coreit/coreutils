/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU General
 * Public License Version 2 only ("GPL") or the Common Development and Distribution
 * License("CDDL") (collectively, the "License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html or nbbuild/licenses/CDDL-GPL-2-CP. See the
 * License for the specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header Notice in
 * each file and include the License file at nbbuild/licenses/CDDL-GPL-2-CP.  Oracle
 * designates this particular file as subject to the "Classpath" exception as
 * provided by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the License Header,
 * with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original Software
 * is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL or only the
 * GPL Version 2, indicate your decision by adding "[Contributor] elects to include
 * this software in this distribution under the [CDDL or GPL Version 2] license." If
 * you do not indicate a single choice of license, a recipient has the option to
 * distribute your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above. However, if
 * you add GPL Version 2 code and therefore, elected the GPL Version 2 license, then
 * the option applies only if the new code is made subject to such option by the
 * copyright holder.
 */
package com.coreit.coreutils.persistence;

import com.coreit.coreutils.StringUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Pack200;
import java.util.jar.Pack200.Unpacker;
import java.util.zip.CRC32;

/**
 *
 * @author Kirill Sorokin
 */
public final class FileUtils {

    /////////////////////////////////////////////////////////////////////////////////
    // Static
    public static final String FILE_SEPARATOR
            = System.getProperty("file.separator");//NOI18N
    private final static Unpacker unpacker = Pack200.newUnpacker();

    public static void copyFile(File source, File destination) throws IOException {
        InputStream in = new FileInputStream(source);
        try (OutputStream out = new FileOutputStream(source)) {
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            out.flush();
            out.close();
        }
    }

    public static OutputStream openOutputStream(File file) throws FileNotFoundException {
        return new FileOutputStream(file);
    }

    public static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }

        in.close();
        out.flush();
        out.close();
    }

    // file/stream read/write ///////////////////////////////////////////////////////
    public static String readFile(
            final File file, String charset) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, charset);
        final Reader reader = new BufferedReader(isr);
        try {
            final char[] buffer = new char[BUFFER_SIZE];
            final StringBuilder stringBuilder = new StringBuilder();
            int readLength;
            while ((readLength = reader.read(buffer)) != -1) {
                stringBuilder.append(buffer, 0, readLength);
            }
            return stringBuilder.toString();
        } finally {
            try {
                reader.close();
                isr.close();
                fis.close();
            } catch (IOException ignord) {
            }
        }
    }

    public static String readFile(String name) throws IOException {
        File file = new File(name);
        return readFile(file);
    }

    public static String readResourceFile(Class clazz, String name) throws IOException {
        ClassLoader classLoader = clazz.getClassLoader();
        BufferedReader rd = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(name)));
        String line;
        String out = "";
        while ((line = rd.readLine()) != null) {
            out += line.trim();
        }
        return out;
    }

    public static String readFile(
            final File file) throws IOException {
        final Reader reader = new BufferedReader(new FileReader(file));
        try {
            final char[] buffer = new char[BUFFER_SIZE];
            final StringBuilder stringBuilder = new StringBuilder();
            int readLength;
            while ((readLength = reader.read(buffer)) != -1) {
                stringBuilder.append(buffer, 0, readLength);
            }
            return stringBuilder.toString();
        } finally {
            try {
                reader.close();
            } catch (IOException ignord) {
            }
        }
    }

    public static String readFirstLine(
            final File file) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        try {
            return reader.readLine();
        } finally {
            try {
                reader.close();
            } catch (IOException ignord) {
            }
        }
    }

    public static List<String> readStringList(
            final File file, String charset) throws IOException {
        final List<String> list = new ArrayList<String>();
        for (String line : StringUtils.splitByLines((readFile(file, charset)))) {
            list.add(line);
        }
        return list;
    }

    public static List<String> readStringList(
            final File file) throws IOException {
        final List<String> list = new ArrayList<String>();
        for (String line : StringUtils.splitByLines((readFile(file)))) {
            list.add(line);
        }
        return list;
    }

    // file metadata ////////////////////////////////////////////////////////////////
    public static Date getLastModified(
            final File file) {
        if (!exists(file)) {
            return null;
        }
        Date date = null;
        try {
            long modif = file.lastModified();
            date = new Date(modif);
        } catch (SecurityException ex) {
            ex = null;
        }
        return date;
    }

    public static long getSize(
            final File file) {
        long size = 0;

        if (file != null && exists(file)) {
            try {
                if (file.isDirectory()) {
                    File[] files = file.listFiles();
                    if (files != null) {
                        for (File f : files) {
                            size += getSize(f);
                        }
                    }
                } else {
                    size = file.length();
                }
            } catch (SecurityException e) {

            }
        }

        return size;
    }

    public static Set<File> getRecursiveFileSet(
            final File file) throws IOException {
        Set<File> fileSet = new HashSet();

        if (file != null && exists(file)) {
            computeRecursiveFileSet(file, fileSet);
        }

        return fileSet;
    }

    static void computeRecursiveFileSet(final File file, Set<File> fileSet) throws IOException {
        try {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                if (files != null) {
                    if (files.length > 0) {
                        fileSet.add(file);
                    }
                    for (File f : files) {
                        computeRecursiveFileSet(f, fileSet);
                    }
                }
            } else {
                fileSet.add(file);
            }
        } catch (SecurityException e) {

        }
    }

    public static long getCrc32(final File file) throws IOException {
        InputStream input = null;
        try {
            input = new FileInputStream(file);
            return getCrc32(input);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ignord) {
                }
            }
        }
    }

    public static long getCrc32(final InputStream input) throws IOException {
        CRC32 crc = new CRC32();
        final byte[] buffer = new byte[BUFFER_SIZE];
        int readLength;
        while ((readLength = input.read(buffer)) != -1) {
            crc.update(buffer, 0, readLength);
        }
        return crc.getValue();
    }

    public static String getMd5(
            final File file) throws IOException {
        return StringUtils.asHexString(getMd5Bytes(file));
    }

    public static String getMd5(
            final InputStream input) throws IOException {
        return StringUtils.asHexString(getMd5Bytes(input));
    }

    public static byte[] getMd5Bytes(
            final File file) throws IOException {
        try {
            return getDigestBytes(file, MD5_DIGEST_NAME);
        } catch (NoSuchAlgorithmException e) {

        }

        return null;
    }

    public static byte[] getMd5Bytes(
            final InputStream input) throws IOException {
        try {
            return getDigestBytes(input, MD5_DIGEST_NAME);
        } catch (NoSuchAlgorithmException e) {

        }

        return null;
    }

    public static String getSha1(
            final File file) throws IOException {
        return StringUtils.asHexString(getSha1Bytes(file));
    }

    public static byte[] getSha1Bytes(
            final File file) throws IOException {
        try {
            return getDigestBytes(file, SHA1_DIGEST_NAME);
        } catch (NoSuchAlgorithmException e) {
        }

        return null;
    }

    public static byte[] getDigestBytes(
            final File file,
            final String algorithm) throws IOException, NoSuchAlgorithmException {
        InputStream input = null;
        try {
            input = new FileInputStream(file);
            return getDigestBytes(input, algorithm);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {

                }
            }
        }
    }

    public static byte[] getDigestBytes(
            final InputStream input,
            final String algorithm) throws IOException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        md.reset();

        final byte[] buffer = new byte[BUFFER_SIZE];//todo: here was 10240?? discus
        int readLength;
        while ((readLength = input.read(buffer)) != -1) {
            md.update(buffer, 0, readLength);
        }

        return md.digest();
    }

    public static boolean isEmpty(
            final File file) {
        if (!exists(file)) {
            return true;
        }

        if (file.isDirectory()) {
            File[] list = file.listFiles();
            if (list != null) {
                for (File child : list) {
                    if (!isEmpty(child)) {
                        return false;
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public static boolean isJarFile(
            final File file) {
        if (file.getName().endsWith(JAR_EXTENSION)) {
            JarFile jar = null;
            try {
                jar = new JarFile(file);
                return true;
            } catch (IOException e) {

                return false;
            } finally {
                if (jar != null) {
                    try {
                        jar.close();
                    } catch (IOException e) {
                    }
                }
            }
        } else {
            return false;
        }
    }

    public static boolean isSigned(
            final File file) throws IOException {
        JarFile jar = new JarFile(file);

        try {
            Enumeration<JarEntry> entries = jar.entries();
            boolean signatureInfoPresent = false;
            boolean signatureFilePresent = false;
            while (entries.hasMoreElements()) {
                String entryName = entries.nextElement().getName();
                if (entryName.startsWith("META-INF/")) {
                    if (entryName.endsWith(".RSA") || entryName.endsWith(".DSA")) {
                        signatureFilePresent = true;
                        if (signatureInfoPresent) {
                            break;
                        }
                    } else if (entryName.endsWith(".SF")) {
                        signatureInfoPresent = true;
                        if (signatureFilePresent) {
                            break;
                        }
                    }
                }
            }
            return signatureFilePresent && signatureInfoPresent;
        } finally {
            jar.close();
        }
    }

    public static boolean exists(
            final File file) {
        if (file.exists()) {
            return true;
        } else if (!file.isFile() && !file.isDirectory()) {
            final File parent = file.getParentFile();
            if ((parent == null) || !parent.exists()) {
                return false;
            }

            final File[] children = parent.listFiles();
            if (children == null) {
                return false;
            }

            for (File child : children) {
                if (child.equals(file)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean isParent(
            final File candidate,
            final File file) {
        File parent = file.getParentFile();

        while ((parent != null) && !candidate.equals(parent)) {
            parent = parent.getParentFile();
        }

        return (parent != null) && candidate.equals(parent);
    }

    public static File getRoot(
            final File fileRequested,
            final List<File> roots) {
        File result = null;
        File file = fileRequested;
        try {
            file = file.getCanonicalFile();
        } catch (IOException e) {

        }
        for (File root : roots) {
            if (isParent(root, file) || root.equals(file)) {
                if (result == null
                        || (result.getAbsolutePath().length()
                        < root.getAbsolutePath().length())) {
                    result = root;
                }
            }
        }
        if (result == null) {
//            if (SystemUtils.isWindows() && FileUtils.isUNCPath(file.getPath())) {
//                return getRoot(file);
//            }
        }
        return result;
    }

    public static long countChildren(
            final File file) {
        long count = 0;

        if (!file.exists()) {
            return 0;
        } else {
            count++;
        }

        final File[] children = file.listFiles();
        if (children != null) {
            for (File child : children) {
                count += countChildren(child);
            }
        }

        return count;
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Instance
    private FileUtils() {
        // does nothing
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Constants
    public static final int BUFFER_SIZE
            = 65536; // NOMAGI

    public static final String SLASH
            = "/"; // NOI18N
    public static final String BACKSLASH
            = "\\"; // NOI18N
    public static final String METAINF_MASK
            = "META-INF.*"; // NOI18N

    public static final String JAR_EXTENSION
            = ".jar"; // NOI18N
    public static final String PROPERTIES_EXTENSION
            = ".properties"; // NOI18N
    public static final String PACK_GZ_SUFFIX
            = ".pack.gz"; // NOI18N
    public static final String SUN_MICR_RSA
            = "META-INF/SUN_MICR.RSA"; // NOI18N
    public static final String SUN_MICR_SF
            = "META-INF/SUN_MICR.SF"; // NOI18N
    public static final String FILES_LIST_ENTRY
            = "META-INF/files.list";//NOI18N
    public static final String CURRENT
            = "."; // NOI18N
    public static final String PARENT
            = ".."; // NOI18N
    public static final String SHA1_DIGEST_NAME
            = "SHA1";//NOI18N
    public static final String MD5_DIGEST_NAME
            = "MD5";//NOI18N
    public static final String INFO_PLIST_STUB
            = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<!DOCTYPE plist SYSTEM \"file://localhost/System/Library/DTDs/PropertyList.dtd\">\n"
            + "<plist version=\"0.9\">\n"
            + "  <dict>\n"
            + "    \n"
            + "    <key>CFBundleName</key>\n"
            + "    <string>{0}</string>\n"
            + "    \n"
            + "    <key>CFBundleVersion</key>\n"
            + "    <string>{1}</string>\n"
            + "    \n"
            + "    <key>CFBundleExecutable</key>\n"
            + "    <string>{3}</string>\n"
            + "    \n"
            + "    <key>CFBundlePackageType</key>\n"
            + "    <string>APPL</string>\n"
            + "    \n"
            + "    <key>CFBundleShortVersionString</key>\n"
            + "    <string>{2}</string>\n"
            + "    \n"
            + "    <key>CFBundleSignature</key>\n"
            + "    <string>????</string>\n"
            + "    \n"
            + "    <key>CFBundleInfoDictionaryVersion</key>\n"
            + "    <string>6.0</string>\n"
            + "    \n"
            + "    <key>CFBundleIconFile</key>\n"
            + "    <string>{4}</string>\n"
            + "  </dict>\n"
            + "</plist>\n";

    public static final String ERROR_OUTPUT_FILE_ENTRY_KEY
            = "FU.error.output.file.entry";// NOI18N

    public static final String ERROR_OUTPUT_DIR_ENTRY_KEY
            = "FU.error.output.dir.entry";// NOI18N
    public static final String ERROR_CLOSE_STREAM_KEY
            = "FU.error.close.stream";//NOI18N
    public static final String ERROR_SOURCE_NOT_READABLE_KEY
            = "FU.error.source.not.readable";//NOI18N
    public static final String ERROR_DEST_NOT_FILE_KEY
            = "FU.error.dest.not.file";//NOI18N
    public static final String ERROR_DEST_CREATION_KEY
            = "FU.error.dest.creation";//NOI18N
    public static final String ERROR_DEST_NOT_WRITABLE_KEY
            = "FU.error.dest.not.writable";//NOI18N
    public static final String ERROR_CANT_GET_FREE_SPACE_KEY
            = "FU.error.freespace";//NOI18N
    public static final String ERROR_CANT_CLOSE_JAR_KEY
            = "FU.error.cannot.close.jar";//NOI18N
    public static final String ERROR_CANT_CREATE_DIR_EXIST_FILE_KEY
            = "FU.error.cannot.create.dir.exist.file";//NOI18N
    public static final String ERROR_CANT_CREATE_DIR_KEY
            = "FU.error.cannot.create.dir";//NOI18N
    public static final String ERROR_NOT_JAR_FILE_KEY
            = "FU.error.not.jar.file";//NOI18N
    public static final String ERROR_SHA1_NOT_SUPPORTED_KEY
            = "FU.error.sha1.not.supported";//NOI18N
    public static final String ERROR_MD5_NOT_SUPPORTED_KEY
            = "FU.error.md5.not.supported";//NOI18N
    public static final String ERROR_FILE_SECURITY_EXCEPTION_KEY
            = "FU.error.file.security.exception";//NOI18N

    public static final String ERROR_PACK200_FAILED_KEY
            = "FU.error.pack200.failed";//NOI18N
    public static final String ERROR_UNPACK200_FAILED_KEY
            = "FU.error.unpack200.failed";//NOI18N
    public static final String USE_INTERNAL_UNPACK200_PROPERTY
            = "nbi.use.internal.unpack200";//NOI18N
}
