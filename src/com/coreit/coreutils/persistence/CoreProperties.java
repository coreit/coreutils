/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.persistence;

import com.coreit.coreutils.SystemUtils;
import com.coreit.coreutils.logging.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.InvalidPropertiesFormatException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * Enhance the {@code Properties} Java Class with more robust methods which make
 * it more easy to enable and manage persistence on a java application, it can
 * provide informations such as
 * <ul>
 * <li>APP install date</li>
 * <li>User Directory</li>
 * <li>Application Close State</li>
 * </ul>
 *
 * @author Ramadan
 */
public abstract class CoreProperties extends Properties {

    /**
     * A key for User home directory
     */
    public static final String HOME_DIR = System.getProperty("user.home");

    /**
     * A key for Application directory name
     */
    public static String APP_DIR = "user.dir";

    /**
     * A key for Property file path
     */
    public static String PROP_FILE = "prop.file";

    /**
     * A key to check if it is first run
     */
    public static String FIRST_RUN = "app.run.first";

    /**
     * A key for file save type
     */
    public static final String SAVE_TYPE = "save.type";

    public static final String INSTALL_DATE = "app.install.date";

    private boolean cleanShut = false;

    /**
     * load application default properties usually called at first run or when
     * properties file cannot be found it can also be used to reset the
     * application properties
     */
    public abstract void loadDefault();

    /**
     * This method returns the application name, the application name is used to
     * create a directory in which the properties file will be saved.
     *
     * @return application name.
     */
    public abstract String getAppName();

    /**
     * Adds a property listener
     *
     * @param listener
     */
    public void addListener(PropertyListener listener) {
        if (listeners == null) {
            listeners = new LinkedList();
        }
        listeners.add(listener);
    }

    /**
     * Constructor for CoreProperties
     */
    protected CoreProperties() {
        init();
    }

    @Override
    public Object setProperty(String key, String value) {
        Object ret = super.setProperty(key, value);
        if (listeners != null) {
            for (PropertyListener l : listeners) {
                l.onChange(key);
            }
        }
        return ret;
    }

    /**
     *
     */
    public interface PropertyListener {

        /**
         * Thrown to all listeners when a property has changed
         *
         * @param key The key that has changed
         */
        public void onChange(String key);
    }

    private volatile List<PropertyListener> listeners;

    /**
     * Child Classes should override this method and return their own file names
     * by default it returns the name {@code config.ini}
     *
     * @return the properties file name
     */
    public String getPropertyFileName() {
        return "config.ini";
    }

    /**
     * This returns the user directory which by default is a concatenation of
     * {@code HOME_DIR} and {@code Application Name}
     *
     * @return the user directory
     */
    public String getUserDir() {
        return HOME_DIR + FileUtils.FILE_SEPARATOR + getAppName() + FileUtils.FILE_SEPARATOR;
    }

    public String getUserFile(String fileName) {
        return getUserDir() + fileName;
    }

    /**
     * Called when {@code CoreProperties} is initializing
     */
    public abstract void onStart();

    /**
     * Called when property file has been opened and properties from file have
     * been loaded can be used to override some properties.
     */
    public abstract void onOpen();

    /**
     * Called when property file is about to close, it can be used to override
     * some properties before they are written to the properties file.
     */
    public abstract void onClose();

    
    public abstract void onFirstRun();
    /**
     * Check whether an application is running for the first time.By default it
     * returns true which means it is first run, upon first run the property
     * <code>FIRST_RUN</code> will be updated to false causing this method to
     * return false on subsequent calls.
     *
     * @return true if it is the first time an application is running.
     */
    public boolean isFirstRun() {
        return getProperty(FIRST_RUN, "true").equals("true");
    }

    private void init() {
        onStart();
        String userDir = getUserDir();
        setProperty(APP_DIR, userDir);
        setProperty(PROP_FILE, userDir + getPropertyFileName());
        File lock = new File(userDir + "prop.lock");
        cleanShut = !lock.exists();

        Console.log("using " + getProperty(PROP_FILE) + " as properties file.");
        File file = new File(getProperty(PROP_FILE));
        try {

            File folder = new File(getProperty(APP_DIR));
            if (!folder.exists()) {
                folder.mkdirs();
            }
            if (!file.exists()) {
                Console.log("file does not exist creating new...");
                file.createNewFile();
                setProperty(INSTALL_DATE, String.valueOf(new Date().getTime()));
                setProperty(FIRST_RUN, "true");
                loadDefault();
                onFirstRun();
            } else {
                Console.log("loading properties from file...");
                FileInputStream fis = new FileInputStream(file);
                try {
                    loadFromXML(fis);
                } catch (InvalidPropertiesFormatException ex) {
                    Console.log(ex);
                    load(fis);
                }
                Console.log("properties loaded successfully.");
            }
            if (!lock.exists()) {
                lock.createNewFile();
            }
            lock.deleteOnExit();
        } catch (IOException ex) {
            loadDefault();
            Console.logError(ex);
        }
        onOpen();
        SystemUtils.addShutdownHook(new Runnable() {
            @Override
            public void run() {
                try {
                    Console.log("saving properties for " + getAppName());
                    setProperty(FIRST_RUN, "false");
                    onClose();
                    if (getProperty(SAVE_TYPE, "Xml").equals("Xml")) {
                        saveAsXml();
                    } else {
                        saveAsKeys();
                    }
                } catch (IOException ex) {
                    Console.log(ex);
                }
            }
        });
        //Runtime.getRuntime().addShutdownHook();
    }

    public boolean wasCleanShut() {
        return cleanShut;
    }

    /**
     * Saves the properties to file as {@code xml} file.
     *
     * @throws IOException
     */
    public final void saveAsXml() throws IOException {
        storeToXML(new FileOutputStream(getUserDir() + getPropertyFileName()), new Date().toString());
    }

    /**
     *
     * @return
     */
    public String getCleanName() {
        if (getAppName().isEmpty()) {
            return "App";
        } else {
            if (getAppName().startsWith(".")) {
                return getAppName().substring(1, getAppName().length()).toUpperCase();
            } else {
                return getAppName();
            }
        }
    }

    /**
     * Save property file as normal key value pair properties file
     */
    public final void saveAsKeys() {

        FileOutputStream fo = null;
        try {
            fo = new FileOutputStream(getUserDir() + getPropertyFileName());
            store(fo, new Date().toString());
        } catch (IOException ex) {
            Console.log(ex);
        } finally {
            try {
                fo.close();
            } catch (IOException ex) {
                Console.log(ex);
            }
        }
    }
}
