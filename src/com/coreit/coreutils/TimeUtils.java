/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils;

import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Ramadan
 */
public class TimeUtils {

    /**
     *
     */
    public interface countdownListener {

        /**
         * called when the count down has ended
         */
        public void countEnd();
    }

    public interface intervalListener {

        public void run();
    }

    /**
     * Count down from the supplied {@code count} to zero in interval specified
     * by {@code period} then notifies the countdown listener
     *
     * @param listener the count listener class
     * @param count the maximum number to count from
     * @param period period between counts in milliseconds
     * @return
     */
    public static Timer countDown(final countdownListener listener, final int count, int period) {

        final Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            int dcount = count;

            @Override
            public void run() {
                if (dcount == 0) {
                    listener.countEnd();
                }
                dcount--;
            }
        }, 0, period);
        return timer;
    }

    public static Runnable setInterval(final intervalListener listener, final int interval) {        
        return new Runnable() {
            @Override
            public void run() {
                while (true) {
                    listener.run();
                    try {
                        Thread.sleep(interval);
                    } catch (InterruptedException ex) {

                    }
                }
            }

        };
    }
}
