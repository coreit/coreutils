/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.threading;

/**
 *
 * @author Ramadan
 */
public class ResourceCreationException extends Exception {

    public ResourceCreationException(String msg) {
        super(msg);
    }

    public ResourceCreationException(String msg, Throwable error) {
        super(msg, error);
    }
    
    public ResourceCreationException(Throwable cause) {
        super(cause);
    }
}
