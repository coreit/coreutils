/*
 * Copyright 2015 Ramadan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.coreit.coreutils.threading;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Ramadan
 * @param <T>
 */
public abstract class ResourcePool<T> {

    int MAX_RESOURCES = 30;
    private final Semaphore sem = new Semaphore(MAX_RESOURCES, true);
    private final Queue<T> resources = new ConcurrentLinkedQueue<>();

    protected abstract T createResource();

    public T getResource(long maxWaitMillis)
            throws InterruptedException, ResourceCreationException {

        // First, get permission to take or create a resource
        sem.tryAcquire(maxWaitMillis, TimeUnit.MILLISECONDS);

        // Then, actually take one if available...
        T res = resources.poll();
        if (res != null) {
            return res;
        }

        // ...or create one if none available
        try {
            return createResource();
        } catch (Exception e) {
            // Don't hog the permit if we failed to create a resource!
            sem.release();
            throw new ResourceCreationException(e);
        }
    }

    public void returnResource(T res) {
        resources.add(res);
        sem.release();
    }
}
