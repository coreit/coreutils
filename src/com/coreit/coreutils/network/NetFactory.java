/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.network;

import com.coreit.coreutils.logging.Console;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 *
 * @author Ramadan
 */
public class NetFactory {

    /**
     *
     */
    public static final String MOZILLA_AGENT_5_0 = "Mozilla/5.0";

    /**
     *
     */
    public static final String CHROME_AGENT = "";

    /**
     *
     */
    public static final String WEBKIT_AGENT = "";

    /**
     *
     */
    public static final String DEFAULT_AGENT = MOZILLA_AGENT_5_0;
    
    private static Pattern VALID_IPV4_PATTERN = null;
    private static Pattern VALID_IPV6_PATTERN = null;
    private static final String ipv4Pattern = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
    private static final String ipv6Pattern = "([0-9a-f]{1,4}:){7}([0-9a-f]){1,4}";
    
    public interface addressWatcher {
        
        public void unreachable();
        
        public void reachable();
    }
    
    static {
        try {
            VALID_IPV4_PATTERN = Pattern.compile(ipv4Pattern, Pattern.CASE_INSENSITIVE);
            VALID_IPV6_PATTERN = Pattern.compile(ipv6Pattern, Pattern.CASE_INSENSITIVE);
        } catch (PatternSyntaxException e) {
            Console.log(e);
        }
    }

    /**
     *
     * @param url
     * @param userAgent
     * @return
     * @throws Exception
     */
    public static String sendGet(String url, String userAgent) throws Exception {
        
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", userAgent);
        
        int responseCode = con.getResponseCode();
        
        Console.log("Sending 'GET' request to URL : " + url);
        Console.log("Response Code : " + responseCode);
        Console.log("Content Encoding : " + con.getContentEncoding());
        Console.log("Content Type : " + con.getContentType());
        
        
        StringBuilder response;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }
        return response.toString();
    }

    /**
     *
     * @param url
     * @return
     * @throws Exception
     */
    public static String sendGet(String url) throws Exception {
        return sendGet(url, DEFAULT_AGENT);
    }

    /**
     *
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    public static String sendPost(String url, String params) throws Exception {
        return sendPost(url, DEFAULT_AGENT, params);
    }

    /**
     *
     * @param ip
     * @return
     */
    public static boolean isIpv4(String ip) {
        Matcher ip4 = VALID_IPV4_PATTERN.matcher(ip);
        return ip4.matches();
    }

    /**
     *
     * @param ip
     * @return
     */
    public static boolean isIpv6(String ip) {
        Matcher ip6 = VALID_IPV6_PATTERN.matcher(ip);
        return ip6.matches();
    }

    /**
     *
     * @param ip
     * @return
     */
    public static boolean isIpAddress(String ip) {
        
        if (isIpv4(ip)) {
            return true;
        } else {
            return isIpv6(ip);
        }
    }

    /**
     *
     * @param address
     * @param timeout
     * @return
     */
    public static boolean ping(String address, int timeout) {
        try {
            InetAddress ip = InetAddress.getByName(address);
            boolean reachable = ip.isReachable(timeout);
            return reachable;
        } catch (IOException ex) {
            Console.log(ex);
            return false;
        }
    }

    /**
     *
     * @param address
     * @param timeout
     * @return
     */
    public static boolean ping(InetAddress address, int timeout) {
        try {
            boolean reachable = address.isReachable(timeout);
            return reachable;
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     *
     * @param ip
     * @return
     */
    public static boolean ping(String ip) {
        return ping(ip, 10000);
    }
    
    public static Timer watchAddress(final String address, long period, final addressWatcher watcher) {
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            
            @Override
            public void run() {
                if (ping(address)) {
                    watcher.reachable();
                } else {
                    watcher.unreachable();
                }
            }
        }, new Date(), period
        );
        return t;
    }

    /**
     *
     * @param url
     * @param userAgent
     * @param urlParameters
     * @return
     * @throws Exception
     */
    public static String sendPost(String url, String userAgent, String urlParameters) throws Exception {
        URL Url = new URL(url);
        HttpURLConnection con = (HttpURLConnection) Url.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", userAgent);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        
        int responseCode = con.getResponseCode();
        
        Console.log("Sending 'POST' request to URL : " + url);
        Console.log("Post parameters : " + urlParameters);
        Console.log("Response Code : " + responseCode);
        
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        
        return response.toString();
        
    }

    /**
     *
     * @param url
     * @param parameters
     * @return
     * @throws Exception
     */
    public static String sendPost(String url, Map<String, String> parameters) throws Exception {
        StringBuilder sb = new StringBuilder();
        for (String param : parameters.keySet()) {
            sb.append(param).append("=").append(parameters.get(param)).append("&");
        }
        return sendPost(url, DEFAULT_AGENT, sb.substring(0, sb.length() - 1));
    }

    /**
     *
     * @param args
     */
    public static void main(String... args) {
        try {
            String name = "RAMA";
            Integer karibusmspro = 1;
            String API_KEY = "21591817515";
            String API_SECRET = "3f78ebd952fad4b14d795149558edcd5ee911bc5";
            
            String phone_number = "255714825469";
            String message = "hello world";
            String resp = sendGet("http://localhost:8080/img/");//sendPost("http://karibusms.com/api/", "api_key=" + API_KEY + "&karibusmspro=" + karibusmspro + "&message=" + message + "&phone_number=" + phone_number + "&name=" + name + "&api_secret=" + API_SECRET);
            Console.log(resp);
        } catch (Exception ex) {
            Logger.getLogger(NetFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
