/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.network;

import com.coreit.coreutils.logging.Console;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 *
 * @author Ramadan
 */
public class WebUtils {

    public static String getHtmlContent(String url) {
        String content = null;
        URLConnection connection;
        try {
            connection = new URL(url).openConnection();
            Scanner scanner = new Scanner(connection.getInputStream());
            scanner.useDelimiter("\\Z");
            content = scanner.next();
        } catch (Exception ex) {
            Console.log(ex);
        }
        return content;
    }
}
