/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2012 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU General
 * Public License Version 2 only ("GPL") or the Common Development and Distribution
 * License("CDDL") (collectively, the "License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html or nbbuild/licenses/CDDL-GPL-2-CP. See the
 * License for the specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header Notice in
 * each file and include the License file at nbbuild/licenses/CDDL-GPL-2-CP.  Oracle
 * designates this particular file as subject to the "Classpath" exception as
 * provided by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the License Header,
 * with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original Software
 * is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL or only the
 * GPL Version 2, indicate your decision by adding "[Contributor] elects to include
 * this software in this distribution under the [CDDL or GPL Version 2] license." If
 * you do not indicate a single choice of license, a recipient has the option to
 * distribute your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above. However, if
 * you add GPL Version 2 code and therefore, elected the GPL Version 2 license, then
 * the option applies only if the new code is made subject to such option by the
 * copyright holder.
 */
package com.coreit.coreutils;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.prefs.Preferences;

/**
 *
 * @author Kirill Sorokin
 */
public final class SystemUtils {

    private static ExecutorService ser;
    private static Thread exitThread;
    private static List<Runnable> runnables;
    /////////////////////////////////////////////////////////////////////////////////
    // Static
    private static Map<String, String> environment
            = new ProcessBuilder().environment();

    // system info //////////////////////////////////////////////////////////////////
    @Deprecated
    public static File getUserHomeDirectory() {
        return new File(USER_HOME);
    }

    public static boolean removeShutdownHook(int hookIndex) {
        if (runnables != null) {
            runnables.remove(hookIndex);
            return true;
        }
        return false;
    }

    public static int addShutdownHook(Runnable runnable) {
        if (runnables == null) {
            runnables = new LinkedList();
        }
        if (exitThread == null) {
            exitThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    if (runnables != null) {
                        for (Runnable runner : runnables) {
                            runner.run();
                        }
                    }
                }

            });
            Runtime.getRuntime().addShutdownHook(exitThread);
        }
        int count = runnables.size();
        runnables.add(runnable);
        return count;
    }

    public static File getDefaultUserdirRoot() {
        String root = System.getProperty("user.home", null);
        if (root != null && root.isEmpty()) {
            return new File(root);
        }
        return null;
    }

    public static String getUserName() {
        return System.getProperty("user.name");
    }

    public static boolean isCurrentUserAdmin() {
        Preferences prefs = Preferences.systemRoot();
        PrintStream systemErr = System.err;
        synchronized (systemErr) {    // better synchroize to avoid problems with other threads that access System.err
            System.setErr(null);
            try {
                prefs.put("foo", "bar"); // SecurityException on Windows
                prefs.remove("foo");
                prefs.flush(); // BackingStoreException on Linux
                return true;
            } catch (Exception e) {
                return false;
            } finally {
                System.setErr(systemErr);
            }
        }
    }

    public static File getCurrentDirectory() {
        return new File(".");
    }

    public static File getTempDirectory() {
        return new File(System.getProperty("java.io.tmpdir"));
    }

    public static File getCurrentJavaHome() {
        return new File(JAVA_HOME);
    }

    public static boolean is64Bit() {
        boolean is64bit = false;
        if (isWindows()) {
            //is64bit = (System.getenv("ProgramFiles(x86)") != null);
            String arch = System.getenv("PROCESSOR_ARCHITECTURE");
            String wow64Arch = System.getenv("PROCESSOR_ARCHITEW6432");

            return arch.endsWith("64")
                    || wow64Arch != null && wow64Arch.endsWith("64");
        } else {
            is64bit = (System.getProperty("os.arch").contains("64"));
        }
        return is64bit;
    }

    public static boolean isCurrentJava64Bit() {
        final String osArch = System.getProperty("os.arch");
        return "64".equals(System.getProperty("sun.arch.data.model"))
                || "64".equals(System.getProperty("com.ibm.vm.bitmode")) || //IBM`s JDK
                osArch.equals("ia64") || //Windows/Linux/? on Intel Itanium
                osArch.toLowerCase(Locale.ENGLISH).equals("ia64w") || //HP-UX on Intel Itanium
                osArch.equals("PA_RISC2.0W") || //HP-UX on PA-RISC 2.0
                osArch.equals("amd64")
                || osArch.equals("sparcv9")
                || osArch.equals("x86_64")
                || osArch.equals("ppc64");
    }

    public static File getPacker() {
        if (isWindows()) {
            return new File(getCurrentJavaHome(), "bin/pack200.exe");
        } else {
            return new File(getCurrentJavaHome(), "bin/pack200");
        }
    }

    public static File getUnpacker() {
        if (isWindows()) {
            return new File(getCurrentJavaHome(), "bin/unpack200.exe");
        } else {
            return new File(getCurrentJavaHome(), "bin/unpack200");
        }
    }

    public static String getLineSeparator() {
        return LINE_SEPARATOR;
    }

    public static String getFileSeparator() {
        return FILE_SEPARATOR;
    }

    public static String getPathSeparator() {
        return PATH_SEPARATOR;
    }

    public static List<File> findIrrelevantFiles(File... parents) throws IOException {
        List<File> list = new LinkedList<File>();

        for (File parent : parents) {
            list.addAll(findIrrelevantFiles(parent));
        }

        return list;
    }

    public static List<File> findExecutableFiles(File... parents) throws IOException {
        List<File> list = new LinkedList<File>();

        for (File parent : parents) {
            list.addAll(findExecutableFiles(parent));
        }

        return list;
    }

    public static void correctFilesPermissions(File... parents) throws IOException {
        for (File file : parents) {
            correctFilesPermissions(file);
        }
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
        }
    }

    public static void setDefaultEnvironment() {
        environment = new ProcessBuilder().environment();
    }

    public static Map<String, String> getEnvironment() {
        return environment;
    }

    // platforms probes /////////////////////////////////////////////////////////////
    public static boolean isWindows() {
        return System.getProperty("os.name").contains("Windows");
    }

    public static boolean isMacOS() {
        return true;
    }

    public static boolean isLinux() {
        return true;
    }

    public static boolean isSolaris() {
        return true;
    }

    public static boolean isUnix() {
        return true;
    }

 

    /////////////////////////////////////////////////////////////////////////////////
    // Constants
    public static final long MAX_EXECUTION_TIME = 10 * 60 * 1000;

    public static final int MAX_DELAY = 50; // NOMAGI
    public static final int INITIAL_DELAY = 5; // NOMAGI
    public static final int DELTA_DELAY = 5; // NOMAGI
    public static final String LINE_SEPARATOR
            = System.getProperty("line.separator");//NOI18N
    public static final String FILE_SEPARATOR
            = System.getProperty("file.separator");//NOI18N
    public static final String PATH_SEPARATOR
            = System.getProperty("path.separator");//NOI18N
    public static final String JAVA_HOME
            = System.getProperty("java.home");//NOI18N
    public static final String USER_HOME
            = System.getProperty("user.home");//NOI18N
    public static final String NO_SPACE_CHECK_PROPERTY
            = "no.space.check";//NOI18N
}
