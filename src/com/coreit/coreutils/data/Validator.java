/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.data;

import com.coreit.coreutils.StringUtils;
import java.util.List;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Ramadan
 */
public class Validator {

    /**
     * Validate if the string passed is an email string
     *
     * @param email
     * @return
     */
    public static boolean isEmail(String email) {
        if (email != null) {
            List<String> li = StringUtils.split(email, "@", true);
            if (li.size() == 2) {
                List l = StringUtils.split(li.get(1), ".", true);
                return l.size() == 2;
            } else {
                return false;
            }
        }
        return false;
    }

    public static boolean isEmail(JTextComponent comp) {
        return isEmail(comp.getText());
    }

    /**
     *
     * @param number
     * @return
     */
    public static boolean isNumber(String number) {
        try {
            Long.valueOf(number);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static boolean isNumber(JTextComponent comp) {
        return isNumber(comp.getText());
    }

    public static boolean isEmpty(JTextComponent comp) {
        if (isNull(comp.getText())) {
            return false;
        }
        return comp.getText().isEmpty();
    }

    public static boolean hasContent(JTextComponent comp) {
        return !isEmpty(comp);
    }

    /**
     *
     * @param string
     * @return
     */
    public static boolean isString(Object string) {
        if (string != null) {
            return string instanceof String;
        }
        return false;
    }

    public static boolean areSimilar(Object src, Object dest) {
        return src.getClass().equals(dest.getClass());
    }

    public static boolean isNull(Object object) {
        return object == null;
    }

    /**
     *
     * @param string
     * @param length
     * @return
     */
    public static boolean hasLength(String string, int length) {
        if (string != null) {
            return string.length() == length;
        }
        return true;
    }
}
