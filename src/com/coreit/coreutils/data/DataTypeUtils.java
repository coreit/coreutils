/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coreit.coreutils.data;

import com.coreit.coreutils.DateUtils;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ramadan
 */
public class DataTypeUtils {

    public static final int NUMERIC = 0x1;
    public static final int STRING = 0x2;
    public static final int DATE = 0x3;
    public static final int BLOB = 0x4;
    public static final int TYPE_ERROR = -1;

    public static boolean equals(Object first, Object second) {
            int type = getType(first);
        switch (type) {
            case DATE: {
                try {
                    return DateUtils.compareDate(first.toString(), second.toString()) == 0;
                } catch (Exception ex) {
                    return false;
                }
            }
            default:
                return first.equals(second);
        }
    }

    public static boolean greaterThan(Object first, Object second) {
        int type = getType(first);
        switch (type) {
            case DATE: {
                try {
                    return DateUtils.compareDate(first.toString(), second.toString()) > 0;
                } catch (Exception ex) {
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean greatOrEqual(Object first, Object second) {
        int type = getType(first);
        switch (type) {
            case DATE: {
                try {
                    return DateUtils.compareDate(first.toString(), second.toString()) >= 0;
                } catch (Exception ex) {
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean lessThan(Object first, Object second) {
        int type = getType(first);
        switch (type) {
            case DATE: {
                try {
                    return DateUtils.compareDate(first.toString(), second.toString()) < 0;
                } catch (Exception ex) {
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean lessOrEqual(Object first, Object second) {
        int type = getType(first);
        switch (type) {
            case DATE: {
                try {
                    return DateUtils.compareDate(first.toString(), second.toString()) <= 0;
                } catch (Exception ex) {
                    return false;
                }
            }
        }
        return false;
    }

    public static int getType(Object value) {
        String date = DateUtils.determineDateFormat(value.toString());
        long numeric = -1;
        if (date != null) {
            return DATE;
        }
        try {
            numeric = Long.valueOf(value.toString());
            return NUMERIC;
        } catch (NumberFormatException ex) {
        }
        return TYPE_ERROR;
    }
}
