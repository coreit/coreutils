# README #

This is collection of various java tools coded and collected from various locations of the net, with the aim of making a life of the Java developer a lot more easier.

### What is this repository for? ###

* The library contains a collection of classes with specific functions for tasks like String manipulations, network requests, data validations and many more. 
* version 0.1
* [Learn Markdown](http://coreutils.coreitz.com)

### How do I get set up? ###

* coreutils have no other dependency
* currently maven is not supported so the option is to add the library to classpath through an IDE or manually
* Most of the current added Classes do not have Javadocs but their method names are self explanatory 

### Who do I talk to? ###

* CoreIt Tanzania
* ramaj93@yahoo.com